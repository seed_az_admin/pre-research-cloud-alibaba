package gang.seed.az.demo.payinterface.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Data
@Accessors(chain = true)
@ApiModel(value = "订单内容vo")
public class OrderDetailVo implements Serializable {

    private static final long serialVersionUID = 8143603259230716605L;

    @ApiModelProperty(required = true, value = "订单明细id")
    private Integer detailId;

    @ApiModelProperty(required = true, value = "订单id")
    private String orderId;

    @ApiModelProperty(required = true, value = "商品id")
    private String goodsId;

    @ApiModelProperty(required = true, value = "商品名称")
    private String goodsName;

    @ApiModelProperty(required = true, value = "规格id")
    private String specId;

    @ApiModelProperty(required = true, value = "规格名称")
    private String specName;

    @ApiModelProperty(required = true, value = "数量")
    private Integer quantity;

    @ApiModelProperty(required = true, value = "商铺id")
    private String shopId;
}