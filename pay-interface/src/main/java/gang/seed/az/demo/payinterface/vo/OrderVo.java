package gang.seed.az.demo.payinterface.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

@Data
@Accessors(chain = true)
@ApiModel(value = "订单vo")
public class OrderVo implements Serializable {

    private static final long serialVersionUID = 7654435515691730752L;

    @ApiModelProperty(required = true, value = "订单id")
    private String orderId;

    @ApiModelProperty(required = true, value = "买家id")
    private String buyerId;

    @ApiModelProperty(required = true, value = "总价格")
    private BigDecimal totalPrice;

    @ApiModelProperty(required = true, value = "订单内容")
    private List<OrderDetailVo> orderList;
}