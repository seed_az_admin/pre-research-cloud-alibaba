package gang.seed.az.demo.payinterface.service;

import gang.seed.az.demo.payinterface.vo.OrderDetailVo;
import gang.seed.az.demo.payinterface.vo.OrderVo;

import java.util.List;

/**
 * @Title: OrderDubboService
 * @Description: TODO
 * @author: duhang
 * @date: 2020-08-17
 */
public interface OrderDubboService {

    /**
     * 支付订单
     * @param goodsList
     * @param buyerId
     * @return
     */
    OrderVo pay(List<OrderDetailVo> goodsList, String buyerId);

    /**
     * 获取买家订单
     * @param buyerId
     * @return
     */
    List<OrderVo> getOrderList(String buyerId);
}