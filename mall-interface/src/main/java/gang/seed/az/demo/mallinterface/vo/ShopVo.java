package gang.seed.az.demo.mallinterface.vo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @Title: ShopVo
 * @Description: TODO
 * 
 * @author: duhang
 * @date: 2020-08-17
 */
@Data
@Accessors(chain = true)
public class ShopVo implements Serializable {

    private static final long serialVersionUID = -8380449866255429695L;

    private String shopId;

    private String shopName;

    private String sellerId;
}