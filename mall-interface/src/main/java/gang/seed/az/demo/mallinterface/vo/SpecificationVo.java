package gang.seed.az.demo.mallinterface.vo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @Title: SpecificationVo
 * @Description: TODO
 * 
 * @author: duhang
 * @date: 2020-08-17
 */
@Data
@Accessors(chain = true)
public class SpecificationVo implements Serializable {

    private static final long serialVersionUID = 8051724237226912251L;

    private String specId;

    private String specName;

    private BigDecimal unitPrice;

    private String goodsId;
}