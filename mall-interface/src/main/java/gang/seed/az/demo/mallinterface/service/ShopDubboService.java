package gang.seed.az.demo.mallinterface.service;

import gang.seed.az.demo.mallinterface.vo.ShopVo;

import java.util.List;

/**
 * @Description TODO
 * @Author duhang
 * @Date 2020-08-17
 */
public interface ShopDubboService {

    /**
     * 创建商铺
     * @param shopName
     * @param selledId
     * @return
     */
    ShopVo add(String shopName, String selledId);

    /**
     * 获取卖家商铺
     * @param sellerId
     * @return
     */
    List<ShopVo> getBySeller(String sellerId);

    /**
     * 获取商铺信息
     * @param shopId
     * @return
     */
    ShopVo getById(String shopId);
}
