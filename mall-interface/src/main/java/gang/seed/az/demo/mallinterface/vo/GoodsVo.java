package gang.seed.az.demo.mallinterface.vo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @Title: GoodsVo
 * @Description: TODO
 * @author: duhang
 * @date: 2020-08-17
 */
@Data
@Accessors(chain = true)
public class GoodsVo implements Serializable {

    private static final long serialVersionUID = -5463705268953848642L;

    private String goodsId;

    private String goodsName;

    private String shopId;
}