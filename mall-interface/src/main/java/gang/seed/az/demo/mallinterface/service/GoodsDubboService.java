package gang.seed.az.demo.mallinterface.service;

import gang.seed.az.demo.mallinterface.vo.GoodsVo;
import gang.seed.az.demo.mallinterface.vo.SpecificationVo;

import java.math.BigDecimal;

/**
 * @Title: GoodsDubboService
 * @Description: TODO
 * 
 * @author: duhang
 * @date: 2020-08-17
 */
public interface GoodsDubboService {

    /**
     * 上架商品
     * @param shopId
     * @param goodsName
     * @return
     */
    GoodsVo add(String shopId, String goodsName);

    /**
     * 下架商品
     * @param goodsId
     */
    void removeGoods(String goodsId);

    /**
     * 获取商品
     * @param goodsId
     * @return
     */
    GoodsVo getGoodsById(String goodsId);

    /**
     * 添加商品规格
     * @param specName
     * @param goodsId
     * @param unitPrice
     * @return
     */
    SpecificationVo add(String specName, String goodsId, BigDecimal unitPrice);

    /**
     * 删除规格
     * @param specId
     */
    void removeSpec(String specId);

    /**
     * 通过获取id获取规格
     * @param specId
     * @return
     */
    SpecificationVo getSpecById(String specId);
}