package gang.seed.az.demo.seata.singledatasource.vo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @Title: DemoPrimarySingleVo
 * @Description: TODO
 * @author: duhang
 * @date: 2020-08-20
 */
@Data
@Accessors(chain = true)
public class DemoPrimarySingleVo implements Serializable {

    private static final long serialVersionUID = -4971451658578595769L;

    private Integer primaryId;

    private String primaryContent;
}