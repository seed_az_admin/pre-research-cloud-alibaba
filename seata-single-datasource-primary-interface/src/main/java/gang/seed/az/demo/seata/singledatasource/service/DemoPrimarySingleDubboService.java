package gang.seed.az.demo.seata.singledatasource.service;

import gang.seed.az.demo.seata.singledatasource.vo.DemoPrimarySingleVo;

/**
 * @Title: DemoPrimarySingleDubboService
 * @Description: TODO
 * @author: duhang
 * @date: 2020-08-20
 */
public interface DemoPrimarySingleDubboService {

    DemoPrimarySingleVo createData(String content);
}