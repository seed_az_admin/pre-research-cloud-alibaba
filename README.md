# 微服务预研示例工程

## 一、技术选型：

- 注册中心、配置中心：`nacos`

- 服务调度：`dubbo`

- 服务构建：`springboot`

- orm：`tkmybatis`

- 分布式事务：`seata`

- 链路追踪：`skywalking` 可配合sentinel、dubbo-admin等使用

## 二、环境搭建(windows为例)

### 1、jdk8

### 2、maven(3.5)

### 3、mysql(5.7.26)

> 按schema文件夹下的sql脚本名称分别创建数据库

![demo-customer-db.png](res/demo-customer-db.png)

![demo-datasource-primary-db.png](res/demo-datasource-primary-db.png)

![demo-datasource-secondary-db.png](res/demo-datasource-secondary-db.png)

![demo-pay-db.png](res/demo-pay-db.png)

![demo-shop-db.png](res/demo-shop-db.png)

![nacos-db.png](res/nacos-db.png)

![seata-db.png](res/seata-db.png)

### 4、influxdb(1.7.9)

- i.下载地址: `https://portal.influxdata.com/downloads/`
- ii.[环境搭建](influxdb.md)

### 5、redis

### 6、nacos(1.3.2)

- i.下载地址: `https://github.com/alibaba/nacos/releases/tag/1.3.2`
- ii.[环境搭建](nacos.md)
- iii.nacos配置中心示例, nacos启动成功后, 登录控制台, 进入配置列表页新增如下配置:

![nacos-config-新增配置.png](res/nacos-config-新增配置.png)

![nacos-config-配置示例.png](res/nacos-config-配置示例.png)

```
# data id
nacos-config-dev.yml

# group
seed-az-demo

# 配置格式
yaml

# 配置内容
spring:
  redis:
    host: 127.0.0.1
    port: 6379
    password:
    database: 7
    timeout: 10000
    lettuce:
      pool:
        max-active: 200
        max-wait: -1
        max-idle: 20
        min-idle: 1
        time-between-eviction-runs: 30
  datasource:
    type: com.alibaba.druid.pool.DruidDataSource
    url: jdbc:mysql://127.0.0.1:3308/demo-datasource-primary?useSSL=false&useUnicode=true&characterEncoding=UTF-8&allowMultiQueries=true&autoReconnect=true&autoReconnectForPools=true&pinGlobalTxToPhysicalConnection=true
    username: root
    password: root
    driverClassName: com.mysql.jdbc.Driver
    # 初始化大小，最小，最大
    initialSize: 1
    minIdle: 1
    maxActive: 10
    # 配置获取连接等待超时的时间
    maxWait: 60000
    # 配置间隔多久才进行一次检测，检测需要关闭的空闲连接，单位是毫秒
    timeBetweenEvictionRunsMillis: 60000
    # 配置一个连接在池中最小生存的时间，单位是毫秒
    minEvictableIdleTimeMillis: 300000
    validationQuery: SELECT 1 FROM DUAL
    testWhileIdle: true
    testOnBorrow: false
    testOnReturn: false
    # 打开PSCache，并且指定每个连接上PSCache的大小
    poolPreparedStatements: true
    maxOpenPreparedStatements: 20
    # connectionProperties: druid.stat.logSlowSql=true;druid.stat.slowSqlMillis=1000;druid.wall.logViolation=false;druid.wall.throwException=false
    # 合并多个DruidDataSource的监控数据
    # useGlobalDataSourceStat: true
    # 本项目禁用druid监控
    filter:
      config:
        enabled: false
    web-stat-filter:
      session-stat-enable: false
      enabled: false
    stat-view-servlet:
      enabled: false

server:
  port: 17078
  undertow:
    io-threads: 16
    worker-threads: 256
    buffer-size: 1024
    direct-buffers: true
    max-http-post-size: -1B
  servlet:
    context-path: /nacos-config
```

### 7、skywalking(8.1.0)

- i.下载地址: `https://www.apache.org/dyn/closer.cgi/skywalking/8.1.0/apache-skywalking-apm-8.1.0.tar.gz`
- ii.[环境搭建](skywalking.md)

### 8、seata(1.3.0)

> 建议下载源码及编译好的包, 源码包中包含必要的sql、配置文件及其他脚本

- i.下载地址: `https://github.com/seata/seata/releases/tag/v1.3.0`
- ii.[环境搭建](seata.md)