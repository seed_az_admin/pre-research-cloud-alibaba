package gang.seed.az.demo.orm.vo;

import lombok.Getter;
import lombok.ToString;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * @Title: PageResponseVo
 * @Description: 通用分页返回vo
 * @author: duhang
 * @date: 2020-07-31
 */
@Getter
@ToString
public class PageContentVo<T> implements Serializable {

    private static final long serialVersionUID = -4818679708483554411L;

    /**
     * 分页列表数据
     */
    private List<T> content;
    /**
     * 是否为空列表
     */
    private boolean empty;
    /**
     * 是否首页
     */
    private boolean first;
    /**
     * 是否最后一页
     */
    private boolean last;
    /**
     * 每页数据量
     */
    private long size;
    /**
     * 总数据量
     */
    private long totalElements;
    /**
     * 总页数
     */
    private long totalPages;

    public PageContentVo() {
        this.content = new ArrayList<>();
        this.empty = Boolean.TRUE;
        this.first = Boolean.TRUE;
        this.last = Boolean.TRUE;
        this.size = 10;
        this.totalElements = 0L;
        this.totalPages = 0L;
    }

    public PageContentVo(List<T> content, long totalElements, int pageNum, int pageSize) {
        this.content = null == content ? new ArrayList<>() : content;
        int tmpSize = this.content.size();
        this.empty = tmpSize < 1;
        this.totalElements = Math.max(totalElements, tmpSize);
        pageNum = Math.max(pageNum, 1);
        this.first = pageNum == 1;
        pageSize = Math.max(pageSize, tmpSize);
        this.size = pageSize;
        this.totalPages = BigDecimal.valueOf(this.totalElements).divide(BigDecimal.valueOf(this.size), BigDecimal.ROUND_UP).longValue();
        this.last = this.totalPages < pageNum;
    }
}