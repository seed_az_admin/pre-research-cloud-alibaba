package gang.seed.az.demo.orm.mybatis;

import org.apache.ibatis.annotations.InsertProvider;

import java.util.List;

/**
 * @Title: DMLBatchMapper
 * @Description: 自定义mapper扩展
 * @author: duhang
 * @date: 2020-08-10
 */
@tk.mybatis.mapper.annotation.RegisterMapper
public interface DMLBatchMapper<T> {

    /**
     * 批量插入 id由研发人员在程序中自己生成
     * @param recordList
     * @return
     */
    @InsertProvider(type = DMLBatchProvider.class, method = "dynamicSQL")
    int insertBatch(List<T> recordList);
}