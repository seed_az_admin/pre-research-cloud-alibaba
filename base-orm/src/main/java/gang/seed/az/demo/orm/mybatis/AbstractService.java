package gang.seed.az.demo.orm.mybatis;

import gang.seed.az.demo.orm.vo.PageContentVo;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import java.util.ArrayList;
import java.util.List;

/**
 * @Title: AbstractService
 * @Description: 通用抽象service
 * @author: duhang
 * @date: 2020-07-31
 */
public abstract class AbstractService<T> {

    @Autowired
    protected IBaseMapper<T> mapper;
//    private Class<T> doClass;

//    public AbstractService() {
//        ParameterizedType pt = (ParameterizedType) this.getClass().getGenericSuperclass();
//        doClass = (Class<T>) pt.getActualTypeArguments()[0];
//    }

    /**
     * 单条写入
     * @param domain
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public int insert(T domain) {

        if (null != domain) {

            return this.mapper.insert(domain);
        }
        return 0;
    }

    /**
     * 批量写入 适用自增主键
     * @param domains
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public int insertList(List<T> domains) {

        if (null != domains && !domains.isEmpty()) {

            return this.mapper.insertList(domains);
        }
        return 0;
    }

    /**
     * 批量写入 主键自己控制
     * @param domains
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public int insertBatch(List<T> domains) {

        if (null != domains && !domains.isEmpty()) {

            return this.mapper.insertBatch(domains);
        }
        return 0;
    }

    /**
     * 根据主键删除单条
     * @param id
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public int deleteById(Object id) {

        if (null != id && !"".equals(id)) {

            return this.mapper.deleteByPrimaryKey(id);
        }
        return 0;
    }

    /**
     * 根据主键批量删除
     * @param ids
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public int deleteByIds(List<?> ids) {

        if (null != ids) {

            return this.mapper.deleteByIdList(ids);
        }
        return 0;
    }

    /**
     * 根据实体类条件删除
     * - 条件判断相等
     * @param domain
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public int delete(T domain) {

        if (null != domain) {

            this.mapper.delete(domain);
        }
        return 0;
    }

    /**
     * 根据条件删除
     * @param condition
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public int deleteByCondition(Example condition) {

        if (null != condition) {

            return this.mapper.deleteByExample(condition);
        }
        return 0;
    }

    /**
     * 根据主键更新属性不为null的值
     * @param domain
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public int updateById(T domain) {

        if (null != domain) {

            return this.mapper.updateByPrimaryKeySelective(domain);
        }
        return 0;
    }

    /**
     * 根据主键更新, null值会被更新
     * @param domain
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public int updateByIdWithEmpty(T domain) {

        if (null != domain) {

            return this.mapper.updateByPrimaryKey(domain);
        }
        return 0;
    }

    /**
     * 根据condition条件更新实体domain包含的不是null的属性值
     * @param domain
     * @param condition
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public int updateByCondition(T domain, Example condition) {

        if (null != domain && null != condition) {

            return this.mapper.updateByExampleSelective(domain, condition);
        }
        return 0;
    }

    /**
     * 根据condition条件更新实体domain包含的全部属性, null值会被更新
     * @param domain
     * @param condition
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public int updateByConditionWithEmpty(T domain, Example condition) {

        if (null != domain && null != condition) {

            return this.mapper.updateByExample(domain, condition);
        }
        return 0;
    }

    /**
     * 根据主键获取
     * @param id
     * @return
     */
    public T selectById(Object id) {

        if (null != id && !"".equals(id)) {

            return this.mapper.selectByPrimaryKey(id);
        }
        return null;
    }

    /**
     * 根据实体中的属性进行查询，只能有一个返回值，有多个结果是抛出异常，查询条件使用等号
     * @param domain
     * @return
     */
    public T selectOne(T domain) {

        if (null != domain) {

            return this.mapper.selectOne(domain);
        }
        return null;
    }

    /**
     * 根据条件获取第一个
     * @param condition
     * @return
     */
    public T selectTopOne(Example condition) {

        List<T> doList = null;

        if (null != condition) {

            doList = this.mapper.selectByExampleAndRowBounds(condition, new RowBounds(RowBounds.NO_ROW_OFFSET, 1));
        } else {

            doList = this.mapper.selectByRowBounds(null, new RowBounds(RowBounds.NO_ROW_OFFSET, 1));
        }
        return null == doList || doList.isEmpty() ? null : doList.get(0);
    }

    /**
     * 根据实体类属性值(不为null)相等获取
     * @param domain null则返回全部
     * @return
     */
    public List<T> selectList(T domain) {

        if (null != domain) {

            return this.mapper.select(domain);
        }
        return this.selectAll();
    }

    /**
     * 获取列表
     * @param domain
     * @param offset
     * @param limit
     * @return
     */
    public List<T> selectList(T domain, int offset, int limit) {

        if (offset < 0) throw new RuntimeException("查询起始位置不能小于0");
        if (limit < 1) throw new RuntimeException("查询数量不能小于1");

        return this.mapper.selectByRowBounds(domain, new RowBounds(offset, limit));
    }

    /**
     * 获取列表
     * @param condition
     * @param offset
     * @param limit
     * @return
     */
    public List<T> selectList(Example condition, int offset, int limit) {

        if (offset < 0) throw new RuntimeException("查询起始位置不能小于0");
        if (limit < 1) throw new RuntimeException("查询数量不能小于1");

        return this.mapper.selectByExampleAndRowBounds(condition, new RowBounds(offset, limit));
    }

    /**
     * 根据条件获取
     * @param condition null则返回全部
     * @return
     */
    public List<T> selectList(Example condition) {

        if (null != condition) {

            return this.mapper.selectByExample(condition);
        }
        return this.selectAll();
    }

    /**
     * 获取全部
     * @return
     */
    public List<T> selectAll() {

        return this.mapper.selectAll();
    }

    /**
     * 获取数量
     * @param domain
     * @return
     */
    public int selectCount(T domain) {

        return this.mapper.selectCount(domain);
    }

    /**
     * 获取数量
     * @param condition
     * @return
     */
    public int selectCount(Example condition) {

        return this.mapper.selectCountByExample(condition);
    }

    /**
     * 分页获取
     * @param domain
     * @param pageNum
     * @param limit
     * @return
     */
    public PageContentVo<T> selectPage(T domain, int pageNum, int limit) {

        if (pageNum < 1) throw new RuntimeException("查询起始位置不能小于1");
        if (limit < 1) throw new RuntimeException("查询数量不能小于1");

        int total = this.selectCount(domain);
        List<T> doList = new ArrayList<>();

        if (total > 0) {

            doList = this.selectList(domain, (pageNum - 1) * limit, limit);
        }
        return new PageContentVo<T>(doList, total, pageNum, limit);
    }

    /**
     * 分页获取
     * @param condition
     * @param pageNum
     * @param limit
     * @return
     */
    public PageContentVo<T> selectPage(Example condition, int pageNum, int limit) {

        if (pageNum < 1) throw new RuntimeException("查询起始位置不能小于1");
        if (limit < 1) throw new RuntimeException("查询数量不能小于1");

        int total = this.selectCount(condition);
        List<T> doList = new ArrayList<>();

        if (total > 0) {

            doList = this.selectList(condition, (pageNum - 1) * limit, limit);
        }
        return new PageContentVo<T>(doList, total, pageNum, limit);
    }

    /**
     * 根据主键判断是否存在
     * @param id
     * @return
     */
    public boolean existsById(Object id) {

        if (null != id && !"".equals(id)) {

            return this.mapper.existsWithPrimaryKey(id);
        }
        return Boolean.FALSE;
    }
}