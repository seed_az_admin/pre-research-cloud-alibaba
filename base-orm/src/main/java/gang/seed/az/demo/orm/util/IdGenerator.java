package gang.seed.az.demo.orm.util;

import org.springframework.util.Assert;

import java.io.Serializable;
import java.nio.ByteBuffer;
import java.security.SecureRandom;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * BSON objectid
 * - IdGenerator.newID()
 * - 生成长度为24的有序id
 * <p>
 * Created by duhang on 2019/6/25 9:53
 */
public class IdGenerator implements Comparable<IdGenerator>, Serializable {
    private static final long serialVersionUID = 3670079982654483072L;
    private static final int RANDOM_VALUE1;
    private static final short RANDOM_VALUE2;
    private static final AtomicInteger NEXT_COUNTER = new AtomicInteger(new SecureRandom().nextInt());

    private static final char[] HEX_CHARS = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};
    private final int timestamp;
    private final int counter;
    private final int randomValue1;
    private final short randomValue2;

    static {
        SecureRandom secureRandom = new SecureRandom();
        RANDOM_VALUE1 = secureRandom.nextInt(16777216);
        RANDOM_VALUE2 = (short) secureRandom.nextInt(32768);
    }

    public static String newID() {
        return new IdGenerator((int) (System.currentTimeMillis() / 1000L), RANDOM_VALUE1, RANDOM_VALUE2, NEXT_COUNTER.getAndIncrement() & 0xFFFFFF, false).toHexString();
    }

    private IdGenerator(int timestamp, int randomValue1, short randomValue2, int counter, boolean checkCounter) {
        if ((randomValue1 & 0xFF000000) != 0) {
            throw new IllegalArgumentException("The machine identifier must be between 0 and 16777215 (it must fit in three bytes).");
        }
        if ((checkCounter) && ((counter & 0xFF000000) != 0)) {
            throw new IllegalArgumentException("The counter must be between 0 and 16777215 (it must fit in three bytes).");
        }
        this.timestamp = timestamp;
        this.counter = (counter & 0xFFFFFF);
        this.randomValue1 = randomValue1;
        this.randomValue2 = randomValue2;
    }

    public byte[] toByteArray() {
        ByteBuffer buffer = ByteBuffer.allocate(12);
        putToByteBuffer(buffer);
        return buffer.array();
    }

    public void putToByteBuffer(ByteBuffer buffer) {
        Assert.notNull(buffer, "buffer can not be null");
        Assert.isTrue(buffer.remaining() >= 12, "state should be: buffer.remaining() >=12");

        buffer.put(int3(this.timestamp));
        buffer.put(int2(this.timestamp));
        buffer.put(int1(this.timestamp));
        buffer.put(int0(this.timestamp));
        buffer.put(int2(this.randomValue1));
        buffer.put(int1(this.randomValue1));
        buffer.put(int0(this.randomValue1));
        buffer.put(short1(this.randomValue2));
        buffer.put(short0(this.randomValue2));
        buffer.put(int2(this.counter));
        buffer.put(int1(this.counter));
        buffer.put(int0(this.counter));
    }

    private String toHexString() {
        char[] chars = new char[24];
        int i = 0;
        for (byte b : toByteArray()) {
            chars[(i++)] = HEX_CHARS[(b >> 4 & 0xF)];
            chars[(i++)] = HEX_CHARS[(b & 0xF)];
        }
        return new String(chars);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if ((o == null) || (getClass() != o.getClass())) {
            return false;
        }

        IdGenerator idFactory = (IdGenerator) o;

        if (this.counter != idFactory.counter) {
            return false;
        }
        if (this.timestamp != idFactory.timestamp) {
            return false;
        }

        if (this.randomValue1 != idFactory.randomValue1) {
            return false;
        }

        return this.randomValue2 == idFactory.randomValue2;
    }

    public int hashCode() {
        int result = this.timestamp;
        result = 31 * result + this.counter;
        result = 31 * result + this.randomValue1;
        result = 31 * result + this.randomValue2;
        return result;
    }

    public int compareTo(IdGenerator other) {
        if (other == null) {
            throw new NullPointerException();
        }

        byte[] byteArray = toByteArray();
        byte[] otherByteArray = other.toByteArray();
        for (int i = 0; i < 12; i++) {
            if (byteArray[i] != otherByteArray[i]) {
                return (byteArray[i] & 0xFF) < (otherByteArray[i] & 0xFF) ? -1 : 1;
            }
        }
        return 0;
    }

    private static byte int3(int x) {
        return (byte) (x >> 24);
    }

    private static byte int2(int x) {
        return (byte) (x >> 16);
    }

    private static byte int1(int x) {
        return (byte) (x >> 8);
    }

    private static byte int0(int x) {
        return (byte) x;
    }

    private static byte short1(short x) {
        return (byte) (x >> 8);
    }

    private static byte short0(short x) {
        return (byte) x;
    }

}
