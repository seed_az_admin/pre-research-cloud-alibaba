package gang.seed.az.demo.orm.mybatis;

import tk.mybatis.mapper.common.*;

/**
 * @Title: IBaseMapper
 * @Description: 通用mapper
 * @author: duhang
 * @date: 2020-07-31
 */
public interface IBaseMapper<T> extends BaseMapper<T>,
        MySqlMapper<T>,
        IdListMapper<T>,
        ConditionMapper<T>,
        ExampleMapper<T>,
        RowBoundsMapper<T>,
        DMLBatchMapper<T> {

}