package gang.seed.az.demo.pay.domain;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;

/**
 * @Title: OrderDetailDo
 * @Description: TODO
 * @author: duhang
 * @date: 2020-08-17
 */
@Data
@Accessors(chain = true)
@Table(name = "demo_order_detail")
public class OrderDetailDo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "detail_id")
    private Integer detailId;

    @Column(name = "orderId")
    private String orderId;

    @Column(name = "goods_id")
    private String goodsId;

    @Column(name = "spec_id")
    private String specId;

    @Column(name = "quantity")
    private Integer quantity;

    @Column(name = "shop_id")
    private String shopId;
}