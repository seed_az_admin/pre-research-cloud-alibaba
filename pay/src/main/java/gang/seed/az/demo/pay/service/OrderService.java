package gang.seed.az.demo.pay.service;

import gang.seed.az.demo.orm.mybatis.AbstractService;
import gang.seed.az.demo.pay.domain.OrderDo;
import org.springframework.stereotype.Service;

/**
 * @Title: OrderService
 * @Description: TODO
 * @author: duhang
 * @date: 2020-08-17
 */
@Service
public class OrderService extends AbstractService<OrderDo> {

}