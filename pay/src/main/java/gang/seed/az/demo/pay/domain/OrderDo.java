package gang.seed.az.demo.pay.domain;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;

/**
 * @Title: OrderDo
 * @Description: TODO
 * @author: duhang
 * @date: 2020-08-17
 */
@Data
@Accessors(chain = true)
@Table(name = "demo_order")
public class OrderDo {

    @Id
    @Column(name = "order_id")
    private String orderId;

    @Column(name = "buyer_id")
    private String buyerId;

    @Column(name = "total_price")
    private BigDecimal totalPrice;
}