package gang.seed.az.demo.pay.mapper;

import gang.seed.az.demo.orm.mybatis.IBaseMapper;
import gang.seed.az.demo.pay.domain.OrderDetailDo;

/**
 * @Title: OrderDetailMapper
 * @Description: TODO
 * @author: duhang
 * @date: 2020-08-17
 */
public interface OrderDetailMapper extends IBaseMapper<OrderDetailDo> {

}