package gang.seed.az.demo.pay;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
public class PayApp {

    private static final Logger LOGGER = LoggerFactory.getLogger(PayApp.class);

    public static void main(String[] args) {

        SpringApplication.run(PayApp.class);

        LOGGER.info(">>>>>>>>>>>>>>>>>>>>>>>>支付服务启动完成");
    }
}