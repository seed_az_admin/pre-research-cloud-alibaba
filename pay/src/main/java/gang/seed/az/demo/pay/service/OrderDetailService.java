package gang.seed.az.demo.pay.service;

import gang.seed.az.demo.orm.mybatis.AbstractService;
import gang.seed.az.demo.pay.domain.OrderDetailDo;
import org.springframework.stereotype.Service;

/**
 * @Title: OrderDetailService
 * @Description: TODO
 * @author: duhang
 * @date: 2020-08-17
 */
@Service
public class OrderDetailService extends AbstractService<OrderDetailDo> {

}