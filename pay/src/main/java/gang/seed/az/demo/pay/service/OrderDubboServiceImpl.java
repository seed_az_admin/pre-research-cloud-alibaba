package gang.seed.az.demo.pay.service;

import gang.seed.az.demo.mallinterface.service.GoodsDubboService;
import gang.seed.az.demo.mallinterface.vo.GoodsVo;
import gang.seed.az.demo.mallinterface.vo.SpecificationVo;
import gang.seed.az.demo.pay.domain.OrderDetailDo;
import gang.seed.az.demo.pay.domain.OrderDo;
import gang.seed.az.demo.pay.util.IdGenerator;
import gang.seed.az.demo.payinterface.service.OrderDubboService;
import gang.seed.az.demo.payinterface.vo.OrderDetailVo;
import gang.seed.az.demo.payinterface.vo.OrderVo;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.dubbo.config.annotation.Reference;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * @Title: OrderDubboServiceImpl
 * @Description: TODO
 * @author: duhang
 * @date: 2020-08-17
 */
@Service(timeout = 60 * 1000, version = "1.0.0")
public class OrderDubboServiceImpl implements OrderDubboService {

    @Autowired
    private OrderService orderService;
    @Autowired
    private OrderDetailService orderDetailService;
    @Reference(version = "1.0.0", protocol = "dubbo", mock = "return null")
    private GoodsDubboService goodsDubboService;

    /**
     * 支付订单
     *
     * @param goodsList
     * @param buyerId
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public OrderVo pay(List<OrderDetailVo> goodsList, String buyerId) {

        OrderVo orderVo = null;

        if (CollectionUtils.isNotEmpty(goodsList)
            && StringUtils.isNotBlank(buyerId)) {

            orderVo = new OrderVo();
            orderVo.setOrderList(goodsList);

            List<OrderDetailDo> detailDoList = new ArrayList<>(goodsList.size());

            BigDecimal totalPrice = BigDecimal.ZERO;

            for (OrderDetailVo detailVo : goodsList) {

                OrderDetailDo detailDo = new OrderDetailDo();
                BeanUtils.copyProperties(detailVo, detailDo);

                SpecificationVo specificationVo = this.goodsDubboService.getSpecById(detailDo.getSpecId());
                totalPrice = totalPrice.add(specificationVo.getUnitPrice());

                detailDoList.add(detailDo);
            }
            this.orderDetailService.insertList(detailDoList);

            OrderDo orderDo = new OrderDo();
            orderDo.setBuyerId(buyerId).setTotalPrice(totalPrice).setOrderId(IdGenerator.newID());

            orderVo.setBuyerId(buyerId).setTotalPrice(totalPrice).setOrderId(IdGenerator.newID());

            this.orderService.insert(orderDo);
        }
        return orderVo;
    }

    /**
     * 获取买家订单
     *
     * @param buyerId
     * @return
     */
    @Override
    public List<OrderVo> getOrderList(String buyerId) {

        if (StringUtils.isNotBlank(buyerId)) {

            OrderDo orderQueryParam = new OrderDo().setBuyerId(buyerId);
            List<OrderDo> orderDoList = this.orderService.selectList(orderQueryParam);

            if (CollectionUtils.isNotEmpty(orderDoList)) {

                List<OrderVo> orderVoList = new ArrayList<>(orderDoList.size());

                orderDoList.forEach(orderDo -> {

                    OrderVo orderVo = new OrderVo();
                    BeanUtils.copyProperties(orderDo, orderVo);

                    OrderDetailDo detailQueryParam = new OrderDetailDo().setOrderId(orderDo.getOrderId());
                    List<OrderDetailDo> detailDoList = this.orderDetailService.selectList(detailQueryParam);

                    if (CollectionUtils.isNotEmpty(detailDoList)) {

                        List<OrderDetailVo> orderDetailVoList = new ArrayList<>(detailDoList.size());

                        detailDoList.forEach(detailDo -> {

                            OrderDetailVo detailVo = new OrderDetailVo();
                            BeanUtils.copyProperties(detailDo, detailVo);

                            String goodsId = detailDo.getGoodsId();
                            GoodsVo goodsVo = this.goodsDubboService.getGoodsById(goodsId);
                            if (null != goodsVo) {
                                detailVo.setGoodsName(goodsVo.getGoodsName());
                            }

                            String specId = detailDo.getSpecId();
                            SpecificationVo specificationVo = this.goodsDubboService.getSpecById(specId);
                            if (null != specificationVo) {
                                detailVo.setSpecName(specificationVo.getSpecName());
                            }

                            orderDetailVoList.add(detailVo);
                        });
                        orderVo.setOrderList(orderDetailVoList);
                    }
                    orderVoList.add(orderVo);
                });
                return orderVoList;
            }
        }
        return null;
    }
}