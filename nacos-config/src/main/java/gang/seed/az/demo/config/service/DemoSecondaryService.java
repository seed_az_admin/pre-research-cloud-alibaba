package gang.seed.az.demo.config.service;

import gang.seed.az.demo.config.domain.DemoSecondaryDo;
import gang.seed.az.demo.config.mapper.DemoSecondaryMapper;
import gang.seed.az.demo.orm.mybatis.AbstractService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Title: DemoSecondaryService
 * @Description: TODO
 * @author: duhang
 * @date: 2020-08-18
 */
@Service
public class DemoSecondaryService extends AbstractService<DemoSecondaryDo> {

    @Autowired
    private DemoSecondaryMapper demoSecondaryMapper;
}