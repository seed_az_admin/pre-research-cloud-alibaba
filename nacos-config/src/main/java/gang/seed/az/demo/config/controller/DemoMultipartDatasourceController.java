package gang.seed.az.demo.config.controller;

import gang.seed.az.demo.config.service.DemoMultipartDatasourceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
public class DemoMultipartDatasourceController {

    @Autowired
    private DemoMultipartDatasourceService demoMultipartDatasourceService;

    @GetMapping("/db/getAll")
    public Map<String, Object> getAll() {

        return this.demoMultipartDatasourceService.getAll();
    }

    @PostMapping("/db/createData")
    public Map<String, Object> createData() {

        return this.demoMultipartDatasourceService.createData();
    }
}