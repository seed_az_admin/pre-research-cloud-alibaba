package gang.seed.az.demo.config.service;

import gang.seed.az.demo.config.domain.DemoPrimaryDo;
import gang.seed.az.demo.config.domain.DemoSecondaryDo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Title: DemoMultipartDatasourceService
 * @Description: TODO
 * @author: duhang
 * @date: 2020-08-18
 */
@Service
public class DemoMultipartDatasourceService {

    @Autowired
    private DemoPrimaryService demoPrimaryService;
    @Autowired
    private DemoSecondaryService demoSecondaryService;


    public Map<String, Object> getAll() {

        List<DemoPrimaryDo> primaryDoList = this.demoPrimaryService.selectAll();
        List<DemoSecondaryDo> secondaryDoList = this.demoSecondaryService.selectAll();

        Map<String, Object> data = new HashMap<>();
        data.put("primaryDoList", primaryDoList);
        data.put("secondaryDoList", secondaryDoList);

        return data;
    }

    @Transactional(rollbackFor = Exception.class)
    public Map<String, Object> createData() {

        List<DemoPrimaryDo> primaryDoList = new ArrayList<>();
        List<DemoSecondaryDo> secondaryDoList = new ArrayList<>();

        for (int i = 0; i < 10; i++) {

            DemoPrimaryDo primaryDo = new DemoPrimaryDo().setPrimaryContent("content" + i);
            primaryDoList.add(primaryDo);

            DemoSecondaryDo secondaryDo = new DemoSecondaryDo().setSecondaryContent("content" + i);
            secondaryDoList.add(secondaryDo);
        }
        this.demoPrimaryService.insertList(primaryDoList);
        this.demoSecondaryService.insertList(secondaryDoList);

        return this.getAll();
    }
}