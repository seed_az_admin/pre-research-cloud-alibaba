package gang.seed.az.demo.config.util;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.data.redis.core.RedisCallback;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import javax.annotation.PostConstruct;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * redis工具类
 *
 * @version 1.0
 * @创建日期 2019年1月30日 下午5:24:07
 */
@Component
public class RedisUtil {

    private static final Logger S_LOG = LoggerFactory.getLogger(RedisUtil.class);

    /**
     * 锁标记前缀
     */
    public static final String S_LOCK_KEY_PREFIX = "lock:";
    /**
     * 通用mq消息队列前缀
     */
    public static final String MQ_KEY_PREFIX = "mq:";

    @Autowired
    private RedisTemplate<String, Object> redisTemplate;

    private static RedisUtil baseredisUtil;

    @PostConstruct
    public void init() {
        baseredisUtil = this;
        baseredisUtil.redisTemplate = this.redisTemplate;
    }

    /**
     * 指定缓存失效时间
     *
     * @param key  键
     * @param time 时间(秒)
     * @return
     */
    public static boolean expire(String key, long time) {
        try {
            if (time > 0) {
                baseredisUtil.redisTemplate.expire(key, time, TimeUnit.SECONDS);
            }
            return true;
        } catch (Exception e) {
            S_LOG.error("RedisUtil>>>>>>>expire(String key, long time), errMsg={}", e);
            return false;
        }
    }

    /**
     * 根据key 获取过期时间
     *
     * @param key 键 不能为null
     * @return 时间(秒) 返回0代表为永久有效
     */
    public static long getExpire(String key) {
        return baseredisUtil.redisTemplate.getExpire(key, TimeUnit.SECONDS);
    }

    /**
     * 判断key是否存在
     *
     * @param key 键
     * @return true 存在 false不存在
     */
    public static boolean hasKey(String key) {
        try {
            return baseredisUtil.redisTemplate.hasKey(key);
        } catch (Exception e) {
            S_LOG.error("RedisUtil>>>>>>>hasKey(String key), errMsg={}", e);
            return false;
        }
    }

    /**
     * 删除缓存
     *
     * @param key 可以传一个值 或多个
     */
    @SuppressWarnings("unchecked")
    public static void del(String... key) {
        if (key != null && key.length > 0) {
            if (key.length == 1) {
                baseredisUtil.redisTemplate.delete(key[0]);
            } else {
                baseredisUtil.redisTemplate.delete(CollectionUtils.arrayToList(key));
            }
        }
    }


    /**
     * 根据key前缀批量删除缓存
     *
     * @param keyPrefix 要删除缓存的指定前缀
     */
    public static boolean batchDel(String keyPrefix) {
        boolean flag = false;
        try {
            Set<String> set = baseredisUtil.redisTemplate.keys(keyPrefix + "*");
            Iterator<String> it = set.iterator();
            while (it.hasNext()) {
                String keyStr = it.next();
                del(keyStr);
            }
            flag = true;
        } catch (Exception e) {
            // 删除指定前缀缓存失败
            S_LOG.error("RedisUtil>>>>>>>batchDel(String keyPrefix), errMsg={}", e);
        }
        return flag;
    }

    /**
     * 普通缓存获取
     *
     * @param key 键
     * @return 值
     */
    public static <T> T get(String key) {
        return key == null ? null : (T) baseredisUtil.redisTemplate.opsForValue().get(key);
    }

    /**
     * 普通缓存放入
     *
     * @param key   键
     * @param value 值
     * @return true成功 false失败
     */
    public static boolean set(String key, Object value) {
        try {
            baseredisUtil.redisTemplate.opsForValue().set(key, value);
            return true;
        } catch (Exception e) {
            S_LOG.error("RedisUtil>>>>>>>set(String key, Object value), errMsg={}", e);
            return false;
        }
    }

    /**
     * 普通缓存放入并设置时间
     *
     * @param key   键
     * @param value 值
     * @param time  时间(秒) time要大于0 如果time小于等于0 将设置无限期
     * @return true成功 false 失败
     */
    public static boolean set(String key, Object value, long time) {
        try {
            if (time > 0) {
                baseredisUtil.redisTemplate.opsForValue().set(key, value, time, TimeUnit.SECONDS);
            } else {
                set(key, value);
            }
            return true;
        } catch (Exception e) {
            S_LOG.error("RedisUtil>>>>>>>set(String key, Object value, long time), errMsg={}", e);
            return false;
        }
    }

    /**
     * 递增
     *
     * @param key   键
     * @param delta 要增加几(大于0)
     * @return
     */
    public static long incr(String key, long delta) {
        if (delta < 0) {
            throw new RuntimeException("递增因子必须大于0");
        }
        return baseredisUtil.redisTemplate.opsForValue().increment(key, delta);
    }

    /**
     * 递减
     *
     * @param key   键
     * @param delta 要减少几(小于0)
     * @return
     */
    public static long decr(String key, long delta) {
        if (delta < 0) {
            throw new RuntimeException("递减因子必须大于0");
        }
        return baseredisUtil.redisTemplate.opsForValue().increment(key, -delta);
    }

    /**
     * HashGet
     *
     * @param key  键 不能为null
     * @param item 项 不能为null
     * @return 值
     */
    public static <T> T hget(String key, String item) {
        return (T) baseredisUtil.redisTemplate.opsForHash().get(key, item);
    }

    /**
     * 获取hashKey对应的所有键值
     *
     * @param key 键
     * @return 对应的多个键值
     */
    public static Map<Object, Object> hmget(String key) {
        return baseredisUtil.redisTemplate.opsForHash().entries(key);
    }

    /**
     * 获取hashKey对应的所有值
     *
     * @param key 键
     * @return 对应的多个值
     */
    public static List<Object> hvalsget(String key) {
        return baseredisUtil.redisTemplate.opsForHash().values(key);
    }

    /**
     * HashSet
     *
     * @param key 键
     * @param map 对应多个键值
     * @return true 成功 false 失败
     */
    public static boolean hmset(String key, Map<Object, Object> map) {
        try {
            baseredisUtil.redisTemplate.opsForHash().putAll(key, map);
            return true;
        } catch (Exception e) {
            S_LOG.error("RedisUtil>>>>>>>hmset(String key, Map<String, Object> map), errMsg={}", e);
            return false;
        }
    }

    /**
     * HashSet 并设置时间
     *
     * @param key  键
     * @param map  对应多个键值
     * @param time 时间(秒)
     * @return true成功 false失败
     */
    public static boolean hmset(String key, Map<Object, Object> map, long time) {
        try {
            baseredisUtil.redisTemplate.opsForHash().putAll(key, map);
            if (time > 0) {
                expire(key, time);
            }
            return true;
        } catch (Exception e) {
            S_LOG.error("RedisUtil>>>>>>>hmset(String key, Map<String, Object> map, long time), errMsg={}", e);
            return false;
        }
    }

    /**
     * 向一张hash表中放入数据,如果不存在将创建
     *
     * @param key   键
     * @param item  项
     * @param value 值
     * @return true 成功 false失败
     */
    public static boolean hset(String key, String item, Object value) {
        try {
            baseredisUtil.redisTemplate.opsForHash().put(key, item, value);
            return true;
        } catch (Exception e) {
            S_LOG.error("RedisUtil>>>>>>>hset(String key, String item, Object value), errMsg={}", e);
            return false;
        }
    }

    /**
     * 向一张hash表中放入数据,如果不存在将创建
     *
     * @param key   键
     * @param item  项
     * @param value 值
     * @param time  时间(秒) 注意:如果已存在的hash表有时间,这里将会替换原有的时间
     * @return true 成功 false失败
     */
    public static boolean hset(String key, String item, Object value, long time) {
        try {
            baseredisUtil.redisTemplate.opsForHash().put(key, item, value);
            if (time > 0) {
                expire(key, time);
            }
            return true;
        } catch (Exception e) {
            S_LOG.error("RedisUtil>>>>>>>hset(String key, String item, Object value, long time), errMsg={}", e);
            return false;
        }
    }

    /**
     * 删除hash表中的值
     *
     * @param key  键 不能为null
     * @param item 项 可以使多个 不能为null
     */
    public static void hdel(String key, Object... item) {
        baseredisUtil.redisTemplate.opsForHash().delete(key, item);
    }

    /**
     * 判断hash表中是否有该项的值
     *
     * @param key  键 不能为null
     * @param item 项 不能为null
     * @return true 存在 false不存在
     */
    public static boolean hHasKey(String key, String item) {
        return baseredisUtil.redisTemplate.opsForHash().hasKey(key, item);
    }

    /**
     * hash递增 如果不存在,就会创建一个 并把新增后的值返回
     *
     * @param key  键
     * @param item 项
     * @param by   要增加几(大于0)
     * @return
     */
    public static double hincr(String key, String item, double by) {
        return baseredisUtil.redisTemplate.opsForHash().increment(key, item, by);
    }

    /**
     * hash递减
     *
     * @param key  键
     * @param item 项
     * @param by   要减少记(小于0)
     * @return
     */
    public static double hdecr(String key, String item, double by) {
        return baseredisUtil.redisTemplate.opsForHash().increment(key, item, -by);
    }

    /**
     * 根据key获取Set中的所有值
     *
     * @param key 键
     * @return
     */
    public static Set<Object> sGet(String key) {
        try {
            return baseredisUtil.redisTemplate.opsForSet().members(key);
        } catch (Exception e) {
            S_LOG.error("RedisUtil>>>>>>>sGet(String key), errMsg={}", e);
            return null;
        }
    }

    /**
     * 根据value从一个set中查询,是否存在
     *
     * @param key   键
     * @param value 值
     * @return true 存在 false不存在
     */
    public static boolean sHasKey(String key, Object value) {
        try {
            return baseredisUtil.redisTemplate.opsForSet().isMember(key, value);
        } catch (Exception e) {
            S_LOG.error("RedisUtil>>>>>>>sHasKey(String key, Object value), errMsg={}", e);
            return false;
        }
    }

    /**
     * 将数据放入set缓存
     *
     * @param key    键
     * @param values 值 可以是多个
     * @return 成功个数
     */
    public static long sSet(String key, Object... values) {
        try {
            return baseredisUtil.redisTemplate.opsForSet().add(key, values);
        } catch (Exception e) {
            S_LOG.error("RedisUtil>>>>>>>sSet(String key, Object... values), errMsg={}", e);
            return 0;
        }
    }

    /**
     * 将set数据放入缓存
     *
     * @param key    键
     * @param time   时间(秒)
     * @param values 值 可以是多个
     * @return 成功个数
     */
    public static long sSetAndTime(String key, long time, Object... values) {
        try {
            Long count = baseredisUtil.redisTemplate.opsForSet().add(key, values);
            if (time > 0) {
                expire(key, time);
            }
            return count;
        } catch (Exception e) {
            S_LOG.error("RedisUtil>>>>>>>sSetAndTime(String key, long time, Object... values), errMsg={}", e);
            return 0;
        }
    }

    /**
     * 获取set缓存的长度
     *
     * @param key 键
     * @return
     */
    public static long sGetSetSize(String key) {
        try {
            return baseredisUtil.redisTemplate.opsForSet().size(key);
        } catch (Exception e) {
            S_LOG.error("RedisUtil>>>>>>>sGetSetSize(String key), errMsg={}", e);
            return 0;
        }
    }

    /**
     * 移除值为value的
     *
     * @param key    键
     * @param values 值 可以是多个
     * @return 移除的个数
     */
    public static long setRemove(String key, Object... values) {
        try {
            Long count = baseredisUtil.redisTemplate.opsForSet().remove(key, values);
            return count;
        } catch (Exception e) {
            S_LOG.error("RedisUtil>>>>>>>setRemove(String key, Object... values), errMsg={}", e);
            return 0;
        }
    }

    /**
     * 获取list缓存的内容
     *
     * @param key   键
     * @param start 开始
     * @param end   结束 0 到 -1代表所有值
     * @return
     */
    public static <T> List<T> lGet(String key, long start, long end) {
        try {
            return (List<T>) baseredisUtil.redisTemplate.opsForList().range(key, start, end);
        } catch (Exception e) {
            S_LOG.error("RedisUtil>>>>>>>lGet(String key, long start, long end), errMsg={}", e);
            return null;
        }
    }

    /**
     * 获取list缓存的长度
     *
     * @param key 键
     * @return
     */
    public static long lGetListSize(String key) {
        try {
            return baseredisUtil.redisTemplate.opsForList().size(key);
        } catch (Exception e) {
            S_LOG.error("RedisUtil>>>>>>>lGetListSize(String key), errMsg={}", e);
            return 0;
        }
    }

    /**
     * 通过索引 获取list中的值
     *
     * @param key   键
     * @param index 索引 index>=0时， 0 表头，1 第二个元素，依次类推；index<0时，-1，表尾，-2倒数第二个元素，依次类推
     * @return
     */
    public static <T> T lGetIndex(String key, long index) {
        try {

            return (T) baseredisUtil.redisTemplate.opsForList().index(key, index);
        } catch (Exception e) {
            S_LOG.error("RedisUtil>>>>>>>lGetIndex(String key, long index), errMsg={}", e);
            return null;
        }
    }

    /**
     * 右侧放入一条
     * 将list放入缓存
     *
     * @param key   键
     * @param value 值
     * @return
     */
    public static boolean lrpush(String key, Object value) {
        try {
            baseredisUtil.redisTemplate.opsForList().rightPush(key, value);
            return true;
        } catch (Exception e) {
            S_LOG.error("RedisUtil>>>>>>>lpush(String key, Object value), errMsg={}", e);
            return false;
        }
    }

    /**
     *
     * @Description: 移除左边第一个
     *
     * @param: * @param key
     * @return: boolean
     */
    public static boolean leftPop(String key) {
        try {
            baseredisUtil.redisTemplate.opsForList().leftPop(key);
            return true;
        } catch (Exception e) {
            S_LOG.error("RedisUtil>>>>>>>lpush(String key, Object value), errMsg={}", e);
            return false;
        }
    }

    /**
     *
     * @Description: 将一个插入到列表头部
     *
     * @author: cm
     * @date: 20:54 2020/6/18
     * @param: * @param key
     * @param value
     * @return: boolean
     */
    public static boolean leftPush(String key, Object value) {
        try {
            baseredisUtil.redisTemplate.opsForList().leftPush(key, value);
            return true;
        } catch (Exception e) {
            S_LOG.error("RedisUtil>>>>>>>lpush(String key, Object value), errMsg={}", e);
            return false;
        }
    }


    /**
     * 将list放入缓存
     *
     * @param key   键
     * @param value 值
     * @param time  时间(秒)
     * @return
     */
    public static boolean lpush(String key, Object value, long time) {
        try {
            baseredisUtil.redisTemplate.opsForList().rightPush(key, value);
            if (time > 0) {
                expire(key, time);
            }
            return true;
        } catch (Exception e) {
            S_LOG.error("RedisUtil>>>>>>>lpush(String key, Object value, long time), errMsg={}", e);
            return false;
        }
    }

    /**
     * 将list放入缓存
     *
     * @param key   键
     * @param value 值
     * @return
     */
    public static boolean lSet(String key, List<?> value) {
        try {
            if (!CollectionUtils.isEmpty(value)) {
                value.stream().forEach(val -> baseredisUtil.redisTemplate.opsForList().rightPush(key, val));
            }
            return true;
        } catch (Exception e) {
            S_LOG.error("RedisUtil>>>>>>>lSet(String key, List<?> value), errMsg={}", e);
            return false;
        }
    }

    /**
     * 将list放入缓存
     *
     * @param key   键
     * @param value 值
     * @param time  时间(秒)
     * @return
     */
    public static boolean lSet(String key, List<?> value, long time) {
        try {
            if (!CollectionUtils.isEmpty(value)) {
                value.stream().forEach(val -> baseredisUtil.redisTemplate.opsForList().rightPush(key, val));
            }
            if (time > 0) {
                expire(key, time);
            }
            return true;
        } catch (Exception e) {
            S_LOG.error("RedisUtil>>>>>>>lSet(String key, List<?> value, long time), errMsg={}", e);
            return false;
        }
    }

    /**
     * 根据索引修改list中的某条数据
     *
     * @param key   键
     * @param index 索引
     * @param value 值
     * @return
     */
    public static boolean lUpdateIndex(String key, long index, Object value) {
        try {
            baseredisUtil.redisTemplate.opsForList().set(key, index, value);
            return true;
        } catch (Exception e) {
            S_LOG.error("RedisUtil>>>>>>>lUpdateIndex(String key, long index, Object value), errMsg={}", e);
            return false;
        }
    }

    /**
     * 移除N个值为value
     *
     * @param key   键
     * @param count 移除多少个
     * @param value 值
     * @return 移除的个数
     */
    public static long lRemove(String key, long count, Object value) {
        try {
            Long remove = baseredisUtil.redisTemplate.opsForList().remove(key, count, value);
            return remove;
        } catch (Exception e) {
            S_LOG.error("RedisUtil>>>>>>>lRemove(String key, long count, Object value), errMsg={}", e);
            return 0;
        }
    }

    /**
     * 指定key加锁
     *
     * @param key
     * @param expire 过期时间(毫秒)
     *               - 默认7天
     * @return
     */
    public static boolean tryLock(String key, Long expire) {

        return tryLock(key, expire, 10);
    }

    /**
     * 指定key加锁
     *
     * @param key
     * @param expire 过期时间(毫秒)
     *               - 默认7天
     * @param retry  重试次数
     * @return
     */
    public static boolean tryLock(String key, Long expire, int retry) {

        retry = 10;

        boolean locked = Boolean.FALSE;

        while (!locked && (retry-- > 0)) {

            locked = lock(key, expire);
        }
        return locked;
    }

    /**
     * 指定key加锁
     *
     * @param key
     * @param expire 过期时间(毫秒)
     *               - 默认7天
     * @return
     */
    public static boolean lock(String key, Long expire) {

        String lock = S_LOCK_KEY_PREFIX + key;

        Calendar calendar = Calendar.getInstance();

        calendar.set(Calendar.DAY_OF_YEAR, calendar.get(Calendar.DAY_OF_YEAR) + 7);

        long expireTimestamp = null == expire || 0 == expire ? calendar.getTimeInMillis() : expire;
//        long expireTimestamp = null == expire || 0 == expire ? System.currentTimeMillis() + 10 * 1000 : expire;

        return (Boolean) baseredisUtil.redisTemplate.execute((RedisCallback) connection -> {

            Boolean acquire = connection.setNX(lock.getBytes(), String.valueOf(expireTimestamp).getBytes());

            if (acquire) {

                return true;
            } else {

                byte[] value = connection.get(lock.getBytes());

                if (Objects.nonNull(value) && value.length > 0) {

                    long expireTime = Long.parseLong(new String(value));

                    if (expireTime < System.currentTimeMillis()) {
                        // 如果锁已经过期
                        byte[] oldValue = connection.getSet(lock.getBytes(), String.valueOf(expireTimestamp).getBytes());
                        // 防止死锁
                        return Long.parseLong(new String(oldValue)) < System.currentTimeMillis();
                    }
                }
            }
            return false;
        });
    }

    /**
     * 模糊查询某些key
     *
     * @author zy 2019-09-29
     */
    public static Set<String> keys(String pattern) {
        Set<String> keys = null;
        try {
            keys = baseredisUtil.redisTemplate.keys(pattern);
        } catch (Exception e) {
            S_LOG.error("RedisUtil>>>>>>>keys(String pattern), errMsg={}", e);
        }
        return keys;
    }

    /**
     * 解锁
     *
     * @param key
     */
    public static boolean unLock(String key) {
        return baseredisUtil.redisTemplate.delete(S_LOCK_KEY_PREFIX + key);
    }

    public static RedisTemplate<String, Object> getRedisTemplate() {
        return baseredisUtil.redisTemplate;
    }

    /**
     * 生产消息
     *
     * @param topic - 消息主题 redis key
     * @param msg   - 消息
     */
    public static <T> Long produce(String topic, T msg) {

        Long rtn = null;

        if (StringUtils.isNotBlank(topic) && null != msg) {

            rtn = RedisUtil.getRedisTemplate().opsForList().rightPush(RedisUtil.MQ_KEY_PREFIX + topic, msg);
        }
        return rtn;
    }

    /**
     * 消费消息
     *
     * @param topic
     * @param timeout
     * @param timeUnit
     * @return
     */
    public static <T> T consume(String topic, Long timeout, TimeUnit timeUnit) {

        T msg = null;

        if (StringUtils.isNotBlank(topic)) {

            if (null == timeout || null == timeUnit) {

                msg = (T) RedisUtil.getRedisTemplate().opsForList().leftPop(RedisUtil.MQ_KEY_PREFIX + topic);
            } else {

                msg = (T) RedisUtil.getRedisTemplate().opsForList().leftPop(RedisUtil.MQ_KEY_PREFIX + topic, timeout, timeUnit);
            }
        }
        return msg;
    }


    //*******************************************SortSet*******************************************//

    /**
     * 向集合中增加一条记录,如果这个值已存在，这个值对应的权重将被置为新的权重
     *
     * @param key   键值
     * @param score double 权重
     * @param value 要加入的值
     * @return 状态码 true-成功 false-失败
     */
    public static Boolean zAdd(String key, Object value, double score) {
        return baseredisUtil.redisTemplate.opsForZSet().add(key, value, score);
    }

    /**
     * 获取给定值在集合中的权重
     *
     * @param key   键
     * @param value 值
     * @return double 权重
     */
    public static Double zScore(String key, Object value) {
        try {
            return baseredisUtil.redisTemplate.opsForZSet().score(key, value);
        } catch (Exception e) {
            S_LOG.error("RedisUtil>>>>>>>zScore(String key, Object value), errMsg={}", e);
            return null;
        }
    }

    /**
     * 获取指定值在集合中的位置，集合排序从低到高
     *
     * @param key   键
     * @param value 值
     * @return long 位置
     */
    public static Long zRank(String key, Object value) {
        try {
            return baseredisUtil.redisTemplate.opsForZSet().rank(key, value);
        } catch (Exception e) {
            S_LOG.error("RedisUtil>>>>>>>zRank(String key, Object value), errMsg={}", e);
            return null;
        }
    }


    /**
     * 返回指定权重区间的元素集合
     *
     * @param key 键
     * @param min 上限权重
     * @param max 下限权重
     * @return Set<Object>
     */
    public static Set<Object> zRangeByScore(String key, double min, double max) {
        try {
            return baseredisUtil.redisTemplate.opsForZSet().rangeByScore(key, min, max);
        } catch (Exception e) {
            S_LOG.error("RedisUtil>>>>>>>zRangeByScore(String key, double min, double max), errMsg={}", e);
            return null;
        }
    }


    /**
     * 从集合中删除成员
     *
     * @param key   键
     * @param value 值
     * @return 返回1成功
     */
    public static long zRmove(String key, Object value) {
        try {
            return baseredisUtil.redisTemplate.opsForZSet().remove(key, value);
        } catch (Exception e) {
            S_LOG.error("RedisUtil>>>>>>>zRmove(String key, Object value), errMsg={}", e);
            return 0;
        }
    }


    /**
     * 删除给定权重区间的元素
     *
     * @param key
     * @param min 下限权重(包含)
     * @param max 上限权重(包含)
     * @return 删除的数量
     */
    public static long zRemoveRangeByScore(String key, double min, double max) {
        try {
            return baseredisUtil.redisTemplate.opsForZSet().removeRangeByScore(key, min, max);
        } catch (Exception e) {
            S_LOG.error("RedisUtil>>>>>>>zRemoveRangeByScore(String key, double min, double max), errMsg={}", e);
            return 0;
        }
    }
}
