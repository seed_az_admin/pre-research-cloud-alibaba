package gang.seed.az.demo.config.controller;

import gang.seed.az.demo.config.util.RedisUtil;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RedisController {

    @GetMapping("/redis/get")
    public Object get(String key) {

        return RedisUtil.get(key);
    }

    @PostMapping("redis/createData")
    public Object createData(String key) {

        RedisUtil.set(key, key);

        return RedisUtil.get(key);
    }
}