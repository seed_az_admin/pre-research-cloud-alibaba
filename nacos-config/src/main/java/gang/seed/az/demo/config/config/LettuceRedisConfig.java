package gang.seed.az.demo.config.config;

import lombok.Data;

/**
 * @Title: LettuceRedisConfig
 * @Description: TODO
 * @author: duhang
 * @date: 2020-08-23
 */
@Data
public class LettuceRedisConfig {

    private int database;

    private String host;

    private String password;

    private int port;

    private long timeout;

    private int maxIdle;

    private int minIdle;

    private int maxActive;

    private long maxWait;

    private int timeBetweenEvictionRuns;

}