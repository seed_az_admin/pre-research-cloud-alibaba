package gang.seed.az.demo.config;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.data.redis.RedisAutoConfiguration;
import org.springframework.boot.autoconfigure.data.redis.RedisRepositoriesAutoConfiguration;

@SpringBootApplication(exclude = {
        RedisAutoConfiguration.class,
        RedisRepositoriesAutoConfiguration.class
})
public class NacosConfigApp {

    private static final Logger LOGGER = LoggerFactory.getLogger(NacosConfigApp.class);

    public static void main(String[] args) {

        SpringApplication.run(NacosConfigApp.class);

        LOGGER.info(">>>>>>>>>>>>>>>>>>>>>>>>nacos配置中心示例服务启动完成");
    }
}