package gang.seed.az.demo.config.config;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.data.redis.connection.RedisPassword;
import org.springframework.data.redis.connection.RedisStandaloneConfiguration;
import org.springframework.data.redis.connection.lettuce.LettuceClientConfiguration;
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;
import org.springframework.data.redis.connection.lettuce.LettucePoolingClientConfiguration;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;

import java.time.Duration;

/**
 * redis配置类
 *
 * @version 1.0
 * @创建者 duhang
 * @创建日期 2019年02月11日 15:28:39
 */
@Configuration
public class RedisConfig {

    private static final Logger S_LOG = LoggerFactory.getLogger(RedisConfig.class);

    @RefreshScope
    @Bean(name = "lettuceRedisConfig")
    public LettuceRedisConfig lettuceRedisConfig(Environment environment) {

        LettuceRedisConfig lettuceRedisConfig = new LettuceRedisConfig();
        lettuceRedisConfig.setHost(environment.getProperty("spring.redis.host"));
        lettuceRedisConfig.setPort(Integer.parseInt(environment.getProperty("spring.redis.port")));
        lettuceRedisConfig.setPassword(environment.getProperty("spring.redis.password"));
        lettuceRedisConfig.setDatabase(Integer.parseInt(environment.getProperty("spring.redis.database")));
        lettuceRedisConfig.setTimeout(Long.parseLong(environment.getProperty("spring.redis.timeout")));
        lettuceRedisConfig.setMaxActive(Integer.parseInt(environment.getProperty("spring.redis.lettuce.pool.max-active")));
        lettuceRedisConfig.setMaxWait(Long.parseLong(environment.getProperty("spring.redis.lettuce.pool.max-wait")));
        lettuceRedisConfig.setMaxIdle(Integer.parseInt(environment.getProperty("spring.redis.lettuce.pool.max-idle")));
        lettuceRedisConfig.setMinIdle(Integer.parseInt(environment.getProperty("spring.redis.lettuce.pool.min-idle")));
        lettuceRedisConfig.setTimeBetweenEvictionRuns(Integer.parseInt(environment.getProperty("spring.redis.lettuce.pool.time-between-eviction-runs")));

        return lettuceRedisConfig;
    }

    @RefreshScope
    @Bean(name = "genericObjectPoolConfig")
    public GenericObjectPoolConfig genericObjectPoolConfig(@Qualifier("lettuceRedisConfig") LettuceRedisConfig lettuceRedisConfig) {

        GenericObjectPoolConfig genericObjectPoolConfig = new GenericObjectPoolConfig();
        genericObjectPoolConfig.setMaxIdle(lettuceRedisConfig.getMaxIdle());
        genericObjectPoolConfig.setMinIdle(lettuceRedisConfig.getMinIdle());
        genericObjectPoolConfig.setMaxTotal(lettuceRedisConfig.getMaxActive());
        genericObjectPoolConfig.setMaxWaitMillis(lettuceRedisConfig.getMaxWait());
        genericObjectPoolConfig.setTimeBetweenEvictionRunsMillis(lettuceRedisConfig.getTimeBetweenEvictionRuns() * 1000L);

        return genericObjectPoolConfig;
    }

    @RefreshScope
    @Bean(name = "lettuceConnectionFactory")
    public LettuceConnectionFactory lettuceConnectionFactory(@Qualifier("lettuceRedisConfig") LettuceRedisConfig lettuceRedisConfig,
                                                             @Qualifier("genericObjectPoolConfig") GenericObjectPoolConfig genericObjectPoolConfig) {
        // 单机版配置
        RedisStandaloneConfiguration redisStandaloneConfiguration = new RedisStandaloneConfiguration();
        redisStandaloneConfiguration.setDatabase(lettuceRedisConfig.getDatabase());
        redisStandaloneConfiguration.setHostName(lettuceRedisConfig.getHost());
        redisStandaloneConfiguration.setPort(lettuceRedisConfig.getPort());
        redisStandaloneConfiguration.setPassword(RedisPassword.of(lettuceRedisConfig.getPassword()));

        // 集群版配置
//        RedisClusterConfiguration redisClusterConfiguration = new RedisClusterConfiguration();
//        String[] serverArray = clusterNodes.split(",");
//        Set<RedisNode> nodes = new HashSet<RedisNode>();
//        for (String ipPort : serverArray) {
//            String[] ipAndPort = ipPort.split(":");
//            nodes.add(new RedisNode(ipAndPort[0].trim(), Integer.valueOf(ipAndPort[1])));
//        }
//        redisClusterConfiguration.setPassword(RedisPassword.of(password));
//        redisClusterConfiguration.setClusterNodes(nodes);
//        redisClusterConfiguration.setMaxRedirects(maxRedirects);

        LettuceClientConfiguration clientConfig = LettucePoolingClientConfiguration.builder()
                .commandTimeout(Duration.ofMillis(lettuceRedisConfig.getTimeout()))
                .poolConfig(genericObjectPoolConfig)
                .build();

        LettuceConnectionFactory factory = new LettuceConnectionFactory(redisStandaloneConfiguration, clientConfig);

        return factory;
    }

    @Bean(name = "redisTemplate")
    public RedisTemplate<String, Object> redisTemplate(@Qualifier("lettuceConnectionFactory") LettuceConnectionFactory factory) {

        RedisTemplate<String, Object> template = new RedisTemplate<>();

        template.setConnectionFactory(factory);

        Jackson2JsonRedisSerializer jackson2JsonRedisSerializer = RedisConfig.jackson2JsonRedisSerializer();

        StringRedisSerializer stringRedisSerializer = new StringRedisSerializer();
        // key采用String的序列化方式
        template.setKeySerializer(stringRedisSerializer);
        // hash的key也采用String的序列化方式
        template.setHashKeySerializer(stringRedisSerializer);
        // value序列化方式采用jackson
        template.setValueSerializer(jackson2JsonRedisSerializer);
        // hash的value序列化方式采用jackson
        template.setHashValueSerializer(jackson2JsonRedisSerializer);

        template.afterPropertiesSet();

        return template;
    }

    public static Jackson2JsonRedisSerializer jackson2JsonRedisSerializer() {

        Jackson2JsonRedisSerializer jackson2JsonRedisSerializer = new Jackson2JsonRedisSerializer(Object.class);

        ObjectMapper om = new ObjectMapper();

        om.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.ANY);

        om.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_FINAL);

        jackson2JsonRedisSerializer.setObjectMapper(om);

        return jackson2JsonRedisSerializer;
    }
}
