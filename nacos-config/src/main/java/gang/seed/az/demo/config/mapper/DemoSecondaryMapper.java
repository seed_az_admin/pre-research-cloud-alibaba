package gang.seed.az.demo.config.mapper;

import gang.seed.az.demo.config.domain.DemoSecondaryDo;
import gang.seed.az.demo.orm.mybatis.IBaseMapper;

/**
 * @Title: DemoSecondaryMapper
 * @Description: TODO
 * @author: duhang
 * @date: 2020-08-18
 */
public interface DemoSecondaryMapper extends IBaseMapper<DemoSecondaryDo> {

}