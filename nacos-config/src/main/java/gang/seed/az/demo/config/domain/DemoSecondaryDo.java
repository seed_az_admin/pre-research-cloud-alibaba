package gang.seed.az.demo.config.domain;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;

/**
 * @Title: DemoSecondaryDo
 * @Description: TODO
 * @author: duhang
 * @date: 2020-08-18
 */
@Data
@Accessors(chain = true)
@Table(name = "demo_secondary")
public class DemoSecondaryDo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "secondary_id")
    private Integer secondaryId;

    @Column(name = "secondary_content")
    private String secondaryContent;
}