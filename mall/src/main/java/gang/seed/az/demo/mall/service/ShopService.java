package gang.seed.az.demo.mall.service;

import gang.seed.az.demo.mall.domain.ShopDo;
import gang.seed.az.demo.mall.mapper.ShopMapper;
import gang.seed.az.demo.orm.mybatis.AbstractService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Description TODO
 * @Author duhang
 * @Date 2020-08-17
 */
@Service
public class ShopService extends AbstractService<ShopDo> {

    @Autowired
    private ShopMapper shopMapper;
}
