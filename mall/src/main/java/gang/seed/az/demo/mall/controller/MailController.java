package gang.seed.az.demo.mall.controller;

import gang.seed.az.demo.mall.domain.GoodsDo;
import gang.seed.az.demo.mall.domain.SpecificationDo;
import gang.seed.az.demo.mall.service.GoodsService;
import gang.seed.az.demo.mall.service.SpecificationService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import tk.mybatis.mapper.entity.Example;

import java.util.List;

@Api(tags = {"商城api"})
@RestController
public class MailController {

    @Autowired
    private GoodsService goodsService;
    @Autowired
    private SpecificationService specificationService;

    @ApiOperation(value = "获取商品列表")
    @ApiImplicitParam(
            paramType = "form",
            name = "goodsName",
            value = "商品名称",
            dataType = "String",
            required = true
    )
    @GetMapping("/mall/getGoodsList")
    public List<GoodsDo> getGoodsList(String goodsName) {


        if (StringUtils.isNotBlank(goodsName)) {

            Example example = Example.builder(GoodsDo.class).build();
            example.createCriteria().andLike("goodsName", "%" + goodsName + "%");

            List<GoodsDo> goodsDos = this.goodsService.selectList(example);

            return goodsDos;
        } else {

            List<GoodsDo> goodsDos = this.goodsService.selectAll();

            return goodsDos;
        }
    }

    @ApiOperation(value = "获取规格列表")
    @ApiImplicitParam(
            paramType = "form",
            name = "goodsId",
            value = "商品id",
            dataType = "String",
            required = true
    )
    @GetMapping("/mall/getSpecList")
    public List<SpecificationDo> getSpecList(String goodsId) {

        if (StringUtils.isNotBlank(goodsId)) {

            SpecificationDo queryParam = new SpecificationDo().setGoodsId(goodsId);

            List<SpecificationDo> dataList = this.specificationService.selectList(queryParam);

            return dataList;
        }
        return null;
    }


}