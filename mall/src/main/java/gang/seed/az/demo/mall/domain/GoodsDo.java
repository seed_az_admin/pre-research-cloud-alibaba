package gang.seed.az.demo.mall.domain;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Accessors(chain = true)
@Table(name = "demo_goods")
@ApiModel(value = "商品实体类do")
public class GoodsDo {

    @Id
    @Column(name = "goods_id")
    @ApiModelProperty(required = true, value = "商品id")
    private String goodsId;

    @Column(name = "goods_name")
    @ApiModelProperty(required = true, value = "商品名称")
    private String goodsName;

    @Column(name = "shop_id")
    @ApiModelProperty(required = true, value = "商铺id")
    private String shopId;
}
