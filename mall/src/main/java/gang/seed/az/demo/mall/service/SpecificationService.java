package gang.seed.az.demo.mall.service;

import gang.seed.az.demo.mall.domain.SpecificationDo;
import gang.seed.az.demo.mall.mapper.SpecificationMapper;
import gang.seed.az.demo.orm.mybatis.AbstractService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Description TODO
 * @Author duhang
 * @Date 2020-08-17
 */
@Service
public class SpecificationService extends AbstractService<SpecificationDo> {

    @Autowired
    private SpecificationMapper specificationMapper;
}
