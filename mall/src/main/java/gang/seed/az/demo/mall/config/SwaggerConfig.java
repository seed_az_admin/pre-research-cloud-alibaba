package gang.seed.az.demo.mall.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.oas.annotations.EnableOpenApi;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

/**
 * @Title: SwaggerConfig
 * @Description: swagger文档配置
 * @author: duhang
 * @date: 2020-08-10
 */
@Configuration
@EnableOpenApi
public class SwaggerConfig {

    @Bean
    public Docket openApi() {
        return new Docket(DocumentationType.OAS_30)
                .groupName("商城")
                .apiInfo(new ApiInfoBuilder()
                        .title("商城服务接口文档")
                        .contact(new Contact("都航","","447647896@qq.com"))
                        .version("1.0")
                        .build())
                .select()
                .apis(RequestHandlerSelectors.basePackage("gang.seed.az.demo.mall"))
                .paths(PathSelectors.any())
                .build();
    }

}