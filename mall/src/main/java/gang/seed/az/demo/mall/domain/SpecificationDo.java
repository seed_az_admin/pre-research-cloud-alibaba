package gang.seed.az.demo.mall.domain;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
@Table(name = "demo_specification")
@ApiModel(value = "商品规格实体类do")
public class SpecificationDo {

    @Id
    @Column(name = "spec_id")
    @ApiModelProperty(required = true, value = "商品规格id")
    private String specId;

    @Column(name = "spec_name")
    @ApiModelProperty(required = true, value = "商品规格名称")
    private String specName;

    @Column(name = "unit_price")
    @ApiModelProperty(required = true, value = "单价")
    private BigDecimal unitPrice;

    @Column(name = "goods_id")
    @ApiModelProperty(required = true, value = "商品id")
    private String goodsId;
}
