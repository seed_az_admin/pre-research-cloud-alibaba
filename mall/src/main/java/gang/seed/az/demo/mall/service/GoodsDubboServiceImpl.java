package gang.seed.az.demo.mall.service;

import gang.seed.az.demo.mall.domain.GoodsDo;
import gang.seed.az.demo.mall.domain.SpecificationDo;
import gang.seed.az.demo.mall.util.IdGenerator;
import gang.seed.az.demo.mallinterface.service.GoodsDubboService;
import gang.seed.az.demo.mallinterface.vo.GoodsVo;
import gang.seed.az.demo.mallinterface.vo.SpecificationVo;
import org.apache.commons.lang3.StringUtils;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;

/**
 * @Description TODO
 * @Author duhang
 * @Date 2020-08-17
 */
@Service(timeout = 60 * 1000, version = "1.0.0")
public class GoodsDubboServiceImpl implements GoodsDubboService {

    @Autowired
    private GoodsService goodsService;
    @Autowired
    private SpecificationService specificationService;

    @Transactional(rollbackFor = Exception.class)
    @Override
    public GoodsVo add(String shopId, String goodsName) {

        if (StringUtils.isNoneBlank(shopId, goodsName)) {

            GoodsDo param = new GoodsDo().setShopId(shopId).setGoodsName(goodsName);

            int exists = this.goodsService.selectCount(param);

            if (exists < 1) {

                param.setGoodsId(IdGenerator.newID());

                int count = this.goodsService.insert(param);

                if (count > 0) {

                    GoodsVo vo = new GoodsVo();
                    BeanUtils.copyProperties(param, vo);

                    return vo;
                }
            }
        }
        return null;
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void removeGoods(String goodsId) {

        if (StringUtils.isNotBlank(goodsId)) {

            this.goodsService.deleteById(goodsId);
        }
    }

    @Override
    public GoodsVo getGoodsById(String goodsId) {

        if (StringUtils.isNotBlank(goodsId)) {

            GoodsDo domain = this.goodsService.selectById(goodsId);

            if (null != domain) {

                GoodsVo vo = new GoodsVo();
                BeanUtils.copyProperties(domain, vo);

                return vo;
            }
        }
        return null;
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public SpecificationVo add(String specName, String goodsId, BigDecimal unitPrice) {

        if (StringUtils.isNoneBlank(specName, goodsId) && null != unitPrice) {

            SpecificationDo param = new SpecificationDo().setSpecName(specName).setGoodsId(goodsId);

            int exists = this.specificationService.selectCount(param);

            if (exists < 1) {

                param.setUnitPrice(unitPrice).setSpecId(IdGenerator.newID());

                int count = this.specificationService.insert(param);

                if (count > 0) {

                    SpecificationVo vo = new SpecificationVo();
                    BeanUtils.copyProperties(param, vo);

                    return vo;
                }
            }
        }
        return null;
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void removeSpec(String specId) {

        if (StringUtils.isNotBlank(specId)) {

            this.specificationService.deleteById(specId);
        }
    }

    @Override
    public SpecificationVo getSpecById(String specId) {

        if (StringUtils.isNotBlank(specId)) {

            SpecificationDo domain = this.specificationService.selectById(specId);

            if (null != domain) {

                SpecificationVo vo = new SpecificationVo();
                BeanUtils.copyProperties(domain, vo);

                return vo;
            }
        }
        return null;
    }
}
