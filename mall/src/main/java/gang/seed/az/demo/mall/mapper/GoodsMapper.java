package gang.seed.az.demo.mall.mapper;

import gang.seed.az.demo.mall.domain.GoodsDo;
import gang.seed.az.demo.orm.mybatis.IBaseMapper;

/**
 * @Description TODO
 * @Author duhang
 * @Date 2020-08-17
 */
public interface GoodsMapper extends IBaseMapper<GoodsDo> {
}
