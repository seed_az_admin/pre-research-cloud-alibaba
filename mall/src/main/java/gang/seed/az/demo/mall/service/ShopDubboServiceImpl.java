package gang.seed.az.demo.mall.service;

import gang.seed.az.demo.mall.domain.ShopDo;
import gang.seed.az.demo.mall.util.IdGenerator;
import gang.seed.az.demo.mallinterface.service.ShopDubboService;
import gang.seed.az.demo.mallinterface.vo.ShopVo;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * @Description TODO
 * @Author duhang
 * @Date 2020-08-17
 */
@Service(timeout = 60 * 1000, version = "1.0.0")
public class ShopDubboServiceImpl implements ShopDubboService {

    @Autowired
    private ShopService shopService;

    @Transactional(rollbackFor = Exception.class)
    @Override
    public ShopVo add(String shopName, String selledId) {

        if (StringUtils.isNoneBlank(shopName, selledId)) {

            ShopDo param = new ShopDo().setShopName(shopName).setSellerId(selledId);

            int exists = this.shopService.selectCount(param);

            if (exists < 1) {

                param.setShopId(IdGenerator.newID());

                int count = this.shopService.insert(param);

                if (count > 0) {

                    ShopVo vo = new ShopVo();
                    BeanUtils.copyProperties(param, vo);

                    return vo;
                }
            }
        }
        return null;
    }

    @Override
    public List<ShopVo> getBySeller(String sellerId) {

        if (StringUtils.isNotBlank(sellerId)) {

            ShopDo queryParam = new ShopDo().setSellerId(sellerId);

            List<ShopDo> doList = this.shopService.selectList(queryParam);

            if (CollectionUtils.isNotEmpty(doList)) {

                List<ShopVo> voList = new ArrayList<>(doList.size());

                doList.forEach(domain -> {

                    ShopVo vo = new ShopVo();
                    BeanUtils.copyProperties(domain, vo);

                    voList.add(vo);
                });
                return voList;
            }
        }
        return null;
    }

    @Override
    public ShopVo getById(String shopId) {

        if (StringUtils.isNotBlank(shopId)) {

            ShopDo domain = this.shopService.selectById(shopId);

            if (null != domain) {

                ShopVo vo = new ShopVo();
                BeanUtils.copyProperties(domain, vo);

                return vo;
            }
        }
        return null;
    }
}
