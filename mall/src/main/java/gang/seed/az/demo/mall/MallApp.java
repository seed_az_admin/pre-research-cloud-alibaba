package gang.seed.az.demo.mall;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MallApp {

    private static final Logger LOGGER = LoggerFactory.getLogger(MallApp.class);

    public static void main(String[] args) {

        SpringApplication.run(MallApp.class);

        LOGGER.info(">>>>>>>>>>>>>>>>>>>>>>>>商城服务启动完成");
    }
}