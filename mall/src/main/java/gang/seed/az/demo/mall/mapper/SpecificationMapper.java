package gang.seed.az.demo.mall.mapper;

import gang.seed.az.demo.mall.domain.SpecificationDo;
import gang.seed.az.demo.orm.mybatis.IBaseMapper;

/**
 * @Description TODO
 * @Author duhang
 * @Date 2020-08-17
 */
public interface SpecificationMapper extends IBaseMapper<SpecificationDo> {
}
