package gang.seed.az.demo.mall.domain;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @Description TODO
 * @Author duhang
 * @Date 2020-08-17
 */
@Data
@Accessors(chain = true)
@Table(name = "demo_shop")
public class ShopDo {

    @Id
    @Column(name = "shop_id")
    private String shopId;

    @Column(name = "shop_name")
    private String shopName;

    @Column(name = "seller_id")
    private String sellerId;
}
