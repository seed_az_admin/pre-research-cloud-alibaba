package gang.seed.az.demo.seller.config;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 日期转换
 */
@Component
public class DateConvert implements Converter<String, Date> {

    private static final Logger S_LOG = LoggerFactory.getLogger(DateConvert.class);

    private static final List<String> S_FORMARTS = new ArrayList<>(4);

    static {
        S_FORMARTS.add("yyyy-MM");
        S_FORMARTS.add("yyyy-MM-dd");
        S_FORMARTS.add("yyyy-MM-dd HH:mm");
        S_FORMARTS.add("yyyy-MM-dd HH:mm:ss");
    }

    /** (non-Javadoc)
     * @see Converter#convert(Object)
     */
    @Override
    public Date convert(String source) {

        if (StringUtils.isNotBlank(null == source ? null : source.trim())) {

            String t_val = source.trim();

            if (source.matches("^\\d{4}-\\d{1,2}$")) {

                return parseDate(t_val, S_FORMARTS.get(0));
            } else if (source.matches("^\\d{4}-\\d{1,2}-\\d{1,2}$")) {

                return parseDate(t_val, S_FORMARTS.get(1));
            } else if (source.matches("^\\d{4}-\\d{1,2}-\\d{1,2} {1}\\d{1,2}:\\d{1,2}$")) {

                return parseDate(t_val, S_FORMARTS.get(2));
            } else if (source.matches("^\\d{4}-\\d{1,2}-\\d{1,2} {1}\\d{1,2}:\\d{1,2}:\\d{1,2}$")) {

                return parseDate(t_val, S_FORMARTS.get(3));
            } else {

                throw new IllegalArgumentException("Invalid boolean value '" + source + "'");
            }
        }
        return null;
    }

    private Date parseDate(String dateStr, String format) {
        Date date = null;
        try {
            DateFormat dateFormat = new SimpleDateFormat(format);
            date = dateFormat.parse(dateStr);
        } catch (Exception e) {

        }
        return date;
    }
}
