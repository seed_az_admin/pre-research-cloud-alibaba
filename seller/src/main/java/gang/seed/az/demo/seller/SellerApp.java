package gang.seed.az.demo.seller;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SellerApp {

    private static final Logger LOGGER = LoggerFactory.getLogger(SellerApp.class);

    public static void main(String[] args) {

        SpringApplication.run(SellerApp.class);

        LOGGER.info(">>>>>>>>>>>>>>>>>>>>>>>>卖家服务启动完成");
    }
}