package gang.seed.az.demo.seller.controller;

import gang.seed.az.demo.mallinterface.service.GoodsDubboService;
import gang.seed.az.demo.mallinterface.service.ShopDubboService;
import gang.seed.az.demo.mallinterface.vo.GoodsVo;
import gang.seed.az.demo.mallinterface.vo.ShopVo;
import gang.seed.az.demo.mallinterface.vo.SpecificationVo;
import gang.seed.az.demo.oauthinterface.service.OauthDubboService;
import gang.seed.az.demo.oauthinterface.vo.CustomerVo;
import gang.seed.az.demo.seller.annotation.AuthIgnore;
import gang.seed.az.demo.seller.util.ThreadLocalUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.util.List;

@Api(tags = {"卖家api"})
@RestController
public class SellerController {

    @Reference(version = "1.0.0", protocol = "dubbo", mock = "return null")
    private OauthDubboService oauthDubboService;
    @Reference(version = "1.0.0", protocol = "dubbo", mock = "return null")
    private GoodsDubboService goodsDubboService;
    @Reference(version = "1.0.0", protocol = "dubbo", mock = "return null")
    private ShopDubboService shopDubboService;

    @ApiOperation(value = "卖家注册")
    @ApiImplicitParams(
            value = {
                    @ApiImplicitParam(
                            paramType = "form",
                            name = "name",
                            value = "卖家用户名",
                            dataType = "String",
                            required = true
                    ),
                    @ApiImplicitParam(
                            paramType = "form",
                            name = "password",
                            value = "卖家密码",
                            dataType = "String",
                            required = true
                    )
            }
    )
    @AuthIgnore
    @PostMapping("/registered")
    public String registered(String name, String password) {

        if (StringUtils.isNoneBlank(name, password)) {

            CustomerVo vo = this.oauthDubboService.registered(name, password);

            if (null != vo) {

                String token = this.oauthDubboService.login(name, password);

                return token;
            }
        }
        return null;
    }

    @ApiOperation(value = "卖家登录")
    @ApiImplicitParams(
            value = {
                    @ApiImplicitParam(
                            paramType = "form",
                            name = "name",
                            value = "卖家用户名",
                            dataType = "String",
                            required = true
                    ),
                    @ApiImplicitParam(
                            paramType = "form",
                            name = "password",
                            value = "卖家密码",
                            dataType = "String",
                            required = true
                    )
            }
    )
    @AuthIgnore
    @PostMapping("/login")
    public String login(String name, String password) {

        if (StringUtils.isNoneBlank(name, password)) {

            String token = this.oauthDubboService.login(name, password);

            return token;
        }
        return null;
    }

    @ApiOperation(value = "创建商铺")
    @ApiImplicitParam(
            paramType = "form",
            name = "shopName",
            value = "商铺名称",
            dataType = "String",
            required = true
    )
    @PostMapping("/shop/add")
    public ShopVo addShop(String shopName) {

        if (StringUtils.isNotBlank(shopName)) {

            String sellerId = ThreadLocalUtil.getUserId();

            ShopVo vo = this.shopDubboService.add(shopName, sellerId);

            return vo;
        }
        return null;
    }

    @ApiOperation(value = "获取名下商铺")
    @GetMapping("/shop/getList")
    public List<ShopVo> getShopList() {

        String sellerId = ThreadLocalUtil.getUserId();

        return this.shopDubboService.getBySeller(sellerId);
    }

    @ApiOperation(value = "上架商品")
    @ApiImplicitParams(
            value = {
                    @ApiImplicitParam(
                            paramType = "form",
                            name = "shopId",
                            value = "商铺id",
                            dataType = "String",
                            required = true
                    ),
                    @ApiImplicitParam(
                            paramType = "form",
                            name = "goodsName",
                            value = "商品名称",
                            dataType = "String",
                            required = true
                    )
            }
    )
    @PostMapping("/goods/add")
    public GoodsVo addGoods(String shopId, String goodsName) {

        if (StringUtils.isNotBlank(goodsName)) {

            GoodsVo vo = this.goodsDubboService.add(shopId, goodsName);

            return vo;
        }
        return null;
    }

    @ApiOperation(value = "添加规格")
    @ApiImplicitParams(
            value = {
                    @ApiImplicitParam(
                            paramType = "form",
                            name = "goodsId",
                            value = "商品id",
                            dataType = "String",
                            required = true
                    ),
                    @ApiImplicitParam(
                            paramType = "form",
                            name = "specName",
                            value = "规格名称",
                            dataType = "String",
                            required = true
                    ),
                    @ApiImplicitParam(
                            paramType = "form",
                            name = "unitPrice",
                            value = "单价",
                            dataType = "BigDecimal",
                            required = true
                    )
            }
    )
    @PostMapping("/spec/add")
    public SpecificationVo addSpec(String goodsId, String specName, BigDecimal unitPrice) {

        if (StringUtils.isNoneBlank(goodsId, specName) && null != unitPrice) {

            SpecificationVo vo = this.goodsDubboService.add(specName, goodsId, unitPrice);

            return vo;
        }
        return null;
    }

}