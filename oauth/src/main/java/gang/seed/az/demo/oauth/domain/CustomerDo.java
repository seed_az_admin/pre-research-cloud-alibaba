package gang.seed.az.demo.oauth.domain;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @Title: CustomerDo
 * @Description: TODO
 * @author: duhang
 * @date: 2020-08-17
 */
@Data
@Accessors(chain = true)
@Table(name = "demo_customer")
public class CustomerDo {

    @Id
    @Column(name = "cus_id")
    private String cusId;

    @Column(name = "cus_name")
    private String cusName;

    @Column(name = "cus_password")
    private String cusPassword;
}