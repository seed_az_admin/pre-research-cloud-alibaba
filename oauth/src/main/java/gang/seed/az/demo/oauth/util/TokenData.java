package gang.seed.az.demo.oauth.util;

import io.jsonwebtoken.Claims;
import lombok.Data;
import lombok.experimental.Accessors;
import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * @Description TODO
 * @Author duhang
 * @Date 2020-08-17
 */
@Data
@Accessors(chain = true)
public class TokenData {

    public static final String KEY_USERID = "userId";

    private String userId;

    /**
     * 生成token数据
     *
     * @param tokenMap
     * @return
     */
    public static TokenData buildFromClaims(Claims tokenMap) {

        TokenData tokenData = new TokenData();

        Object userId = tokenMap.get("userId");

        if (null != userId) {

            tokenData.setUserId((String) userId);
        }
        return tokenData;
    }


    public static String generateToken(String userId, long expiration) {

        if (StringUtils.isNotBlank(userId)) {

            Map<String, Object> claims = new HashMap<>();

            claims.put(KEY_USERID, userId);

            String token = JWTUtil.generateToken(claims, expiration);

            return token;
        }
        return null;
    }
}
