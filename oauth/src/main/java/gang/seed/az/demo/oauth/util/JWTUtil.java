package gang.seed.az.demo.oauth.util;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.apache.commons.lang3.StringUtils;

import java.util.Date;
import java.util.Map;

/**
 * @Description TODO
 * @Author duhang
 * @Date 2020-08-17
 */
public class JWTUtil {

    /**
     * token加密串
     */
    private static final String SECRET = "invoice";
    /**
     * 缺省情况下，Token会每5分钟被刷新一次
     */
    private static final long REFRESH_TOKEN_INTERVAL = 5 * 60 * 1000L;
    /**
     * 创建时间
     */
    private static final String CLAIM_KEY_CREATED_TIME = "CreatedTime";

    /**
     * 生成加密后的JWT令牌，生成的结果中包含令牌前缀，如"Bearer "
     *
     * @param claims                令牌中携带的数据
     * @param expirationMillisecond 过期的毫秒数
     * @return 生成后的令牌信息
     * @创建者 duhang
     * @创建日期 2018年10月29日下午2:38:11
     */
    public static String generateToken(Map<String, Object> claims, long expirationMillisecond) {
        // 自动添加token的创建时间
        long createTime = System.currentTimeMillis();

        claims.put(CLAIM_KEY_CREATED_TIME, createTime);

        String token = Jwts.builder().setClaims(claims).setExpiration(new Date(createTime + expirationMillisecond))
                .signWith(SignatureAlgorithm.HS512, SECRET).compact();

        return token;
    }

    /**
     * 生成加密后的JWT令牌
     *
     * @param claims 令牌中携带的数据
     * @return 生成后的令牌信息
     * @创建者 duhang
     * @创建日期 2018年10月29日下午2:38:41
     */
    public static String generateTokenDefaultExpiration(Map<String, Object> claims) {

        return generateToken(claims, 84600000L);
    }

    /**
     * 获取token中的数据对象
     *
     * @param token 令牌信息(需要包含令牌前缀，如"Bearer:")
     * @return 令牌中的数据对象，解析视频返回null。
     * @创建者 duhang
     * @创建日期 2018年10月29日下午2:39:00
     */
    public static Claims parseToken(String token) {

        if (StringUtils.isBlank(token)) {

            return null;
        }
        Claims claims = null;
        try {

            claims = Jwts.parser().setSigningKey(SECRET).parseClaimsJws(token).getBody();
        } catch (Exception e) {

//            throw new SessionException("登录失效，请重新登录。");
        }
        return claims;
    }

    /**
     * 判断解密后的Token payload是否需要被强制刷新，如果需要,则调用generateToken方法重新生成Token。
     *
     * @param claims Token解密后payload数据
     * @return 需要刷新，否则false
     * @创建者 duhang
     * @创建日期 2018年10月29日下午2:39:39
     */
    public static boolean needToRefresh(Claims claims) {

        if (claims == null) {

            return false;
        }
        Long createTime = (Long) claims.get(CLAIM_KEY_CREATED_TIME);

        return createTime != null && System.currentTimeMillis() - createTime > REFRESH_TOKEN_INTERVAL;
    }
}