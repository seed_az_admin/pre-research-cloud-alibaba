package gang.seed.az.demo.oauth.service;

import gang.seed.az.demo.oauth.domain.CustomerDo;
import gang.seed.az.demo.oauth.util.IdGenerator;
import gang.seed.az.demo.oauth.util.JWTUtil;
import gang.seed.az.demo.oauth.util.TokenData;
import gang.seed.az.demo.oauthinterface.service.OauthDubboService;
import gang.seed.az.demo.oauthinterface.vo.CustomerVo;
import io.jsonwebtoken.Claims;
import org.apache.commons.lang3.StringUtils;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @Description TODO
 * @Author duhang
 * @Date 2020-08-17
 */
@Service(timeout = 60 * 1000, version = "1.0.0")
public class OauthDubboServiceImpl implements OauthDubboService {

    @Autowired
    private CustomerService customerService;

    @Override
    public CustomerVo registered(String name, String password) {

        CustomerVo vo = null;

        if (StringUtils.isNoneBlank(name, password)) {

            CustomerDo param = new CustomerDo().setCusName(name);

            int exists = this.customerService.selectCount(param);

            if (exists < 1) {

                param.setCusPassword(password).setCusId(IdGenerator.newID());

                int count = this.customerService.insert(param);

                if (count > 0) {

                    vo = new CustomerVo();

                    BeanUtils.copyProperties(param, vo);
                }
            }
        }
        return vo;
    }

    @Override
    public String login(String name, String password) {

        if (StringUtils.isNoneBlank(name, password)) {

            CustomerDo queryParam = new CustomerDo().setCusName(name).setCusPassword(password);

            CustomerDo exists = this.customerService.selectOne(queryParam);

            if (null != exists) {

                String userId = exists.getCusId();

                String token = TokenData.generateToken(userId, 60 * 60 * 24 * 1000L);

                return token;
            }
        }
        return null;
    }

    @Override
    public CustomerVo checkToken(String token) {

        if (StringUtils.isNotBlank(token)) {

            Claims claims = JWTUtil.parseToken(token);

            if (null != claims) {

                String userId = TokenData.buildFromClaims(claims).getUserId();

                if (StringUtils.isNotBlank(userId)) {

                    CustomerDo exists = this.customerService.selectById(userId);

                    if (null != exists) {

                        CustomerVo vo = new CustomerVo();

                        BeanUtils.copyProperties(exists, vo);

                        return vo;
                    }
                }
            }
        }
        return null;
    }
}
