package gang.seed.az.demo.oauth.service;

import gang.seed.az.demo.oauth.domain.CustomerDo;
import gang.seed.az.demo.oauth.mapper.CustomerMapper;
import gang.seed.az.demo.orm.mybatis.AbstractService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Title: CustomerService
 * @Description: TODO
 * @author: duhang
 * @date: 2020-08-17
 */
@Service
public class CustomerService extends AbstractService<CustomerDo> {

    @Autowired
    private CustomerMapper customerMapper;
}