package gang.seed.az.demo.oauth;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OauthApp {

    private static final Logger LOGGER = LoggerFactory.getLogger(OauthApp.class);

    public static void main(String[] args) {

        SpringApplication.run(OauthApp.class);

        LOGGER.info(">>>>>>>>>>>>>>>>>>>>>>>>鉴权服务启动完成");
    }
}