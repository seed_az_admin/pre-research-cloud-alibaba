package gang.seed.az.demo.oauth.mapper;

import gang.seed.az.demo.oauth.domain.CustomerDo;
import gang.seed.az.demo.orm.mybatis.IBaseMapper;

/**
 * @Title: CustomerMapper
 * @Description: TODO
 * @author: duhang
 * @date: 2020-08-17
 */
public interface CustomerMapper extends IBaseMapper<CustomerDo> {

}