package gang.seed.az.demo.seata.singledatasource.mapper;


import gang.seed.az.demo.seata.singledatasource.domain.DemoSecondaryDo;
import gang.seed.az.demo.seata.singledatasource.mybatis.IBaseMapper;

/**
 * @Title: DemoSecondaryMapper
 * @Description: TODO
 * @author: duhang
 * @date: 2020-08-18
 */
public interface DemoSecondaryMapper extends IBaseMapper<DemoSecondaryDo> {

}