package gang.seed.az.demo.seata.singledatasource.service;

import gang.seed.az.demo.seata.singledatasource.domain.DemoSecondaryDo;
import gang.seed.az.demo.seata.singledatasource.vo.DemoPrimarySingleVo;
import gang.seed.az.demo.seata.singledatasource.vo.DemoSecondarySingleVo;
import io.seata.spring.annotation.GlobalTransactional;
import org.apache.dubbo.config.annotation.Reference;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @Title: DemoSecondarySingleDubboServiceImpl
 * @Description: TODO
 * @author: duhang
 * @date: 2020-08-20
 */
@Service(timeout = 60 * 1000)
public class DemoSecondarySingleDubboServiceImpl implements DemoSecondarySingleDubboService {

    @Autowired
    private DemoSecondaryService demoSecondaryService;
    @Reference
    private DemoPrimarySingleDubboService demoPrimarySingleDubboService;

    @GlobalTransactional(rollbackFor = Exception.class)
    @Override
    public DemoSecondarySingleVo createData(String content) {

        DemoPrimarySingleVo primarySingleVo = demoPrimarySingleDubboService.createData(content);

        DemoSecondarySingleVo vo = null;

        if (null != primarySingleVo) {

            DemoSecondaryDo demoSecondaryDo = new DemoSecondaryDo().setSecondaryContent(content);

            int count = this.demoSecondaryService.insert(demoSecondaryDo);

            if (count > 0) {

                vo = new DemoSecondarySingleVo();
                BeanUtils.copyProperties(demoSecondaryDo, vo);
            }
        }
        return vo;
    }
}