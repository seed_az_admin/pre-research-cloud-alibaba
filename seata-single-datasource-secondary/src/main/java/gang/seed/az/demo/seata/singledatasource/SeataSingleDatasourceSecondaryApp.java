package gang.seed.az.demo.seata.singledatasource;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SeataSingleDatasourceSecondaryApp {

    private static final Logger LOGGER = LoggerFactory.getLogger(SeataSingleDatasourceSecondaryApp.class);

    public static void main(String[] args) {

        SpringApplication.run(SeataSingleDatasourceSecondaryApp.class);

        LOGGER.info(">>>>>>>>>>>>>>>>>>>>>>>>seata单数据源(第二数据源)启动完成");
    }
}