package gang.seed.az.demo.seata.singledatasource.service;

import gang.seed.az.demo.seata.singledatasource.domain.DemoSecondaryDo;
import gang.seed.az.demo.seata.singledatasource.mapper.DemoSecondaryMapper;
import gang.seed.az.demo.seata.singledatasource.mybatis.AbstractService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Title: DemoSecondaryService
 * @Description: TODO
 * @author: duhang
 * @date: 2020-08-18
 */
@Service
public class DemoSecondaryService extends AbstractService<DemoSecondaryDo> {

    @Autowired
    private DemoSecondaryMapper demoSecondaryMapper;
}