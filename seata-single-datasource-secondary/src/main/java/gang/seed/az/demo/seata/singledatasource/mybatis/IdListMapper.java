package gang.seed.az.demo.seata.singledatasource.mybatis;

import org.apache.ibatis.annotations.DeleteProvider;
import org.apache.ibatis.annotations.SelectProvider;

import java.util.List;

/**
 * @Title: IdListMapper
 * @Description: TODO
 * @author: duhang
 * @date: 2020-08-19
 */
@tk.mybatis.mapper.annotation.RegisterMapper
public interface IdListMapper<T> {

    /**
     * 根据主键字符串进行删除，类中只有存在一个带有@Id注解的字段
     *
     * @param ids 如 "1,2,3,4"
     * @return
     */
    @DeleteProvider(type = IdListProvider.class, method = "dynamicSQL")
    int deleteByIdList(List<?> ids);

    /**
     * 根据主键字符串进行查询，类中只有存在一个带有@Id注解的字段
     *
     * @param ids 如 "1,2,3,4"
     * @return
     */
    @SelectProvider(type = IdListProvider.class, method = "dynamicSQL")
    List<T> selectByIdList(List<?> ids);
}