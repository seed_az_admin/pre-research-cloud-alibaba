package gang.seed.az.demo.seata.multidatasource.config.datasource;

import com.alibaba.druid.pool.DruidDataSource;
import io.seata.rm.datasource.DataSourceProxy;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * @Title: DataSourceProxyConfig
 * @Description: TODO
 * @author: duhang
 * @date: 2020-08-20
 */
@Configuration
public class DataSourceProxyConfig {

    @Bean("primaryDatasource")
    @ConfigurationProperties(prefix = "spring.datasource.primary")
    public DruidDataSource primaryDatasource() {
        return new DruidDataSource();
    }

    @Bean("secondaryDatasource")
    @ConfigurationProperties(prefix = "spring.datasource.secondary")
    public DruidDataSource secondaryDatasource() {
        return new DruidDataSource();
    }

    @Bean(name = "primary")
    public DataSourceProxy primaryDataSourceProxy(@Qualifier("primaryDatasource") DruidDataSource primaryDatasource) {
        return new DataSourceProxy(primaryDatasource);
    }

    @Bean(name = "secondary")
    public DataSourceProxy secondaryDataSourceProxy(@Qualifier("secondaryDatasource") DruidDataSource secondaryDatasource) {
        return new DataSourceProxy(secondaryDatasource);
    }

    @Bean(name = "dynamicDataSource")
    public DataSource dynamicDataSource(@Qualifier("primary") DataSourceProxy primaryDataSourceProxy,
                                        @Qualifier("secondary") DataSourceProxy secondaryDataSourceProxy) {

        Map<Object, Object> dataSourceMap = new HashMap<>(3);
        dataSourceMap.put("primary", primaryDataSourceProxy);
        dataSourceMap.put("secondary", secondaryDataSourceProxy);

        DynamicDataSource dynamicDataSource = new DynamicDataSource();
        dynamicDataSource.setTargetDataSources(dataSourceMap);
        dynamicDataSource.setDefaultTargetDataSource(primaryDataSourceProxy);

        DataSourceContextHolder.dataSourceKeys.addAll(dataSourceMap.keySet());

        return dynamicDataSource;
    }


    @Bean(name = "sqlSessionFactoryPrimary")
    public SqlSessionFactory sqlSessionFactoryPrimary(@Qualifier("primaryDatasource") DataSource dataSource)
            throws Exception {
        return createSqlSessionFactory(dataSource);
    }

    @Bean(name = "sqlSessionFactorySecondary")
    public SqlSessionFactory sqlSessionFactorySecondary(@Qualifier("secondaryDatasource") DataSource dataSource)
            throws Exception {
        return createSqlSessionFactory(dataSource);
    }

    @Bean(name = "sqlSessionTemplate")
    public CustomSqlSessionTemplate sqlSessionTemplate(@Qualifier("sqlSessionFactoryPrimary") SqlSessionFactory factoryPrimary,
                                                       @Qualifier("sqlSessionFactorySecondary") SqlSessionFactory factorySecondary) {
        Map<Object,SqlSessionFactory> sqlSessionFactoryMap = new HashMap<>();
        sqlSessionFactoryMap.put("primary", factoryPrimary);
        sqlSessionFactoryMap.put("secondary", factorySecondary);

        CustomSqlSessionTemplate customSqlSessionTemplate = new CustomSqlSessionTemplate(factoryPrimary);
        customSqlSessionTemplate.setDefaultTargetSqlSessionFactory(factoryPrimary);
        customSqlSessionTemplate.setTargetSqlSessionFactorys(sqlSessionFactoryMap);

        return customSqlSessionTemplate;
    }

    @Bean(name = "mapperScannerConfigurer")
    public MyMapperScannerConfigurer mapperScannerConfigurer() {
        MyMapperScannerConfigurer mapperScannerConfigurer = new MyMapperScannerConfigurer();
        mapperScannerConfigurer.setSqlSessionTemplateBeanName("sqlSessionTemplate");
//        mapperScannerConfigurer.setSqlSessionFactoryBeanName("sqlSessionFactory");
        mapperScannerConfigurer.setBasePackage("gang.seed.az.demo.**.mapper");
        // 配置通用Mapper，详情请查阅官方文档
        Properties properties = new Properties();
        properties.setProperty("mappers", "gang.seed.az.demo.seata.multidatasource.mybatis.IBaseMapper");
        properties.setProperty("type-aliases-package", "gang.seed.az.demo.**.domain");
        properties.setProperty("notEmpty", "true");//insert、update是否判断字符串类型!='' 即 test="str != null"表达式内是否追加 and str != ''
        properties.setProperty("IDENTITY", "MYSQL");
        mapperScannerConfigurer.setProperties(properties);
        return mapperScannerConfigurer;
    }

    private SqlSessionFactory createSqlSessionFactory(DataSource dataSource) throws Exception {
        SqlSessionFactoryBean bean = new SqlSessionFactoryBean();
        bean.setDataSource(dataSource);
        ResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
        bean.setConfigLocation(resolver.getResource("mybatis.xml"));
        bean.setMapperLocations(resolver.getResources("classpath:gang/seed/az/demo/**/**Mapper.xml"));
        return bean.getObject();
    }
}