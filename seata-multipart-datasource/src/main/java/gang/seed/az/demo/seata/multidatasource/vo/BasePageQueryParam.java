package gang.seed.az.demo.seata.multidatasource.vo;

import lombok.Getter;
import lombok.ToString;

import java.io.Serializable;

/**
 * @Title BasePageQueryParam
 * @Description: 分页查询基础类
 * @author: duhang
 * @date: 2020-07-02
 */
@Getter
@ToString
public class BasePageQueryParam implements Serializable {

    private static final long serialVersionUID = -2815950490521677513L;
    /**
     * 当前页码
     */
    protected Integer pageNum = 1;
    /**
     * 每页大小
     */
    protected Integer pageSize = 10;

    public BasePageQueryParam() {}
    public BasePageQueryParam(Integer pageNum, Integer pageSize) {
        this.pageNum = null == pageNum || pageNum.equals(0) || pageNum < 0 ? 1 : pageNum;
        this.pageSize = null == pageSize || pageSize.equals(0) || pageSize < 0 ? 10 : pageSize;
    }

    public <T extends BasePageQueryParam> T setPageNum(int pageNum) {
        this.pageNum = pageNum <= 0 ? 1 : pageNum;
        return (T) this;
    }

    public <T extends BasePageQueryParam> T setPageSize(int pageSize) {
        this.pageSize = pageSize <= 0 ? 10 : pageSize;
        return (T) this;
    }

    public int getSqlPageNum() {
        return (this.pageNum - 1) * this.pageSize;
    }

    public int getJPAPageResultPageNum() {
        return (this.pageNum - 1);
    }
}