package gang.seed.az.demo.seata.multidatasource.mybatis;

import org.apache.ibatis.mapping.MappedStatement;
import tk.mybatis.mapper.entity.EntityColumn;
import tk.mybatis.mapper.mapperhelper.EntityHelper;
import tk.mybatis.mapper.mapperhelper.MapperHelper;
import tk.mybatis.mapper.mapperhelper.MapperTemplate;
import tk.mybatis.mapper.mapperhelper.SqlHelper;

import java.util.Set;

/**
 * @Title: DMLBatchProvider
 * @Description: 批量操作扩展接口
 * @author: duhang
 * @date: 2020-08-10
 */
public class DMLBatchProvider extends MapperTemplate {

    public DMLBatchProvider(Class<?> mapperClass, MapperHelper mapperHelper) {
        super(mapperClass, mapperHelper);
    }

    /**
     * 批量插入 id由研发人员在程序中自己生成
     * @param ms
     * @return
     */
    public String insertBatch(MappedStatement ms) {
        final Class<?> entityClass = this.getEntityClass(ms);
        //开始拼sql
        StringBuilder sqlBuilder = new StringBuilder();
        sqlBuilder.append("<bind name=\"listNotEmptyCheck\" value=\"@tk.mybatis.mapper.util.OGNL@notEmptyCollectionCheck(list, '").append(ms.getId()).append(" 方法参数为空')\"/>");
        sqlBuilder.append(SqlHelper.insertIntoTable(entityClass, this.tableName(entityClass), "list[0]"));
        sqlBuilder.append(SqlHelper.insertColumns(entityClass, false, false, false));
        sqlBuilder.append(" VALUES ");
        sqlBuilder.append("<foreach collection=\"list\" item=\"record\" separator=\",\" >");
        sqlBuilder.append("<trim prefix=\"(\" suffix=\")\" suffixOverrides=\",\">");
        //获取全部列
        Set<EntityColumn> columnList = EntityHelper.getColumns(entityClass);
        for (EntityColumn column : columnList) {
            sqlBuilder.append(column.getColumnHolder("record")).append(",");
        }
        sqlBuilder.append("</trim>");
        sqlBuilder.append("</foreach>");
        // 反射把MappedStatement中的设置主键名
        EntityHelper.setKeyProperties(EntityHelper.getPKColumns(entityClass), ms);
        return sqlBuilder.toString();
    }

}