package gang.seed.az.demo.seata.multidatasource.config.datasource;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.GenericBeanDefinition;
import tk.mybatis.mapper.common.Marker;
import tk.mybatis.mapper.mapperhelper.MapperHelper;
import tk.mybatis.mapper.util.StringUtil;

import java.util.Properties;

/**
 * @Title: MyMapperScannerConfigurer
 * @Description: TODO
 * @author: duhang
 * @date: 2020-08-18
 */
public class MyMapperScannerConfigurer extends org.mybatis.spring.mapper.MapperScannerConfigurer {

    private MapperHelper mapperHelper = new MapperHelper();
    private String sqlSessionTemplateBeanName;

    public void setMarkerInterface(Class<?> superClass) {
        super.setMarkerInterface(superClass);
        if (Marker.class.isAssignableFrom(superClass)) {
            mapperHelper.registerMapper(superClass);
        }
    }

    public MapperHelper getMapperHelper() {
        return mapperHelper;
    }

    public void setMapperHelper(MapperHelper mapperHelper) {
        this.mapperHelper = mapperHelper;
    }

    public void setSqlSessionTemplateBeanName(String sqlSessionTemplateBeanName) {
        super.setSqlSessionTemplateBeanName(sqlSessionTemplateBeanName);
        this.sqlSessionTemplateBeanName = sqlSessionTemplateBeanName;
    }

    /**
     * 属性注入
     *
     * @param properties
     */
    public void setProperties(Properties properties) {
        mapperHelper.setProperties(properties);
    }

    @Override
    public void postProcessBeanDefinitionRegistry(BeanDefinitionRegistry registry) {
        super.postProcessBeanDefinitionRegistry(registry);
        String[] names = registry.getBeanDefinitionNames();
        GenericBeanDefinition definition;
        for (String name : names) {
            BeanDefinition beanDefinition = registry.getBeanDefinition(name);
            if (beanDefinition instanceof GenericBeanDefinition) {
                definition = (GenericBeanDefinition) beanDefinition;
                if (StringUtil.isNotEmpty(definition.getBeanClassName())
                        && definition.getBeanClassName().equals("org.mybatis.spring.mapper.MapperFactoryBean")) {
                    definition.setBeanClass(MapperFactoryBean.class);
                    definition.getPropertyValues().add("mapperHelper", this.mapperHelper);
                    definition.getPropertyValues().add("sqlSessionTemplateBeanName", this.sqlSessionTemplateBeanName);
                }
            }
        }
    }
}