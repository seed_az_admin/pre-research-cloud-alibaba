package gang.seed.az.demo.seata.multidatasource;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SeataMultipartDatasourceApp {

    private static final Logger LOGGER = LoggerFactory.getLogger(SeataMultipartDatasourceApp.class);

    public static void main(String[] args) {

        SpringApplication.run(SeataMultipartDatasourceApp.class);

        LOGGER.info(">>>>>>>>>>>>>>>>>>>>>>>>seata多数据源启动完成");
    }
}