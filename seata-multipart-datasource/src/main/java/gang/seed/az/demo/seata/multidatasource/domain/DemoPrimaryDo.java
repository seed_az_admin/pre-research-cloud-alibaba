package gang.seed.az.demo.seata.multidatasource.domain;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;

/**
 * @Title: DemoPrimaryDo
 * @Description: TODO
 * @author: duhang
 * @date: 2020-08-18
 */
@Data
@Accessors(chain = true)
@Table(name = "demo_primary")
public class DemoPrimaryDo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "primary_id")
    private Integer primaryId;

    @Column(name = "primary_content")
    private String primaryContent;
}