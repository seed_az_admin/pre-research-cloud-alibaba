package gang.seed.az.demo.seata.multidatasource.service;

import gang.seed.az.demo.seata.multidatasource.domain.DemoSecondaryDo;
import gang.seed.az.demo.seata.multidatasource.mapper.DemoSecondaryMapper;
import gang.seed.az.demo.seata.multidatasource.mybatis.AbstractService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Title: DemoSecondaryService
 * @Description: TODO
 * @author: duhang
 * @date: 2020-08-18
 */
@Service
public class DemoSecondaryService extends AbstractService<DemoSecondaryDo> {

    @Autowired
    private DemoSecondaryMapper demoSecondaryMapper;
}