package gang.seed.az.demo.seata.multidatasource.service;

import gang.seed.az.demo.seata.multidatasource.domain.DemoPrimaryDo;
import gang.seed.az.demo.seata.multidatasource.domain.DemoSecondaryDo;
import gang.seed.az.demo.seata.singledatasource.service.DemoPrimarySingleDubboService;
import gang.seed.az.demo.seata.singledatasource.service.DemoSecondarySingleDubboService;
import io.seata.spring.annotation.GlobalTransactional;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Title: DemoMultidatasourceService
 * @Description: TODO
 * @author: duhang
 * @date: 2020-08-20
 */
@Service
public class DemoMultidatasourceService {

    @Autowired
    private DemoPrimaryService demoPrimaryService;
    @Autowired
    private DemoSecondaryService demoSecondaryService;
    @Reference
    private DemoPrimarySingleDubboService demoPrimarySingleDubboService;
    @Reference
    private DemoSecondarySingleDubboService demoSecondarySingleDubboService;

    @GlobalTransactional(rollbackFor = Exception.class)
    public Map<String, Object> createAndGetData() {

        this.demoPrimaryService.insert(new DemoPrimaryDo().setPrimaryContent("multi self start"));

        this.demoSecondaryService.insert(new DemoSecondaryDo().setSecondaryContent("multi self start"));

        this.demoPrimarySingleDubboService.createData("seata multipart datasource primary call");

        this.demoSecondarySingleDubboService.createData("seata multipart datasource secondary call");

        this.demoSecondaryService.insert(new DemoSecondaryDo().setSecondaryContent("multi self end"));

        this.demoPrimaryService.insert(new DemoPrimaryDo().setPrimaryContent("multi self end"));

        throw new RuntimeException("测试seata分布式事务");

//        return this.getData();
    }

    public Map<String, Object> getData() {

        List<DemoPrimaryDo> primaryDoList = this.demoPrimaryService.selectAll();

        List<DemoSecondaryDo> secondaryDoList = this.demoSecondaryService.selectAll();

        Map<String, Object> data = new HashMap<>();

        data.put("primaryDoList", primaryDoList);

        data.put("secondaryDoList", secondaryDoList);

        return data;
    }
}