package gang.seed.az.demo.seata.multidatasource.controller;

import gang.seed.az.demo.seata.multidatasource.service.DemoMultidatasourceService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * @Title: DemoMultipartDatasourceController
 * @Description: TODO
 * @author: duhang
 * @date: 2020-08-20
 */
@Api(tags = {"seata多数据源示例api"})
@RestController
public class DemoMultipartDatasourceController {

    @Autowired
    private DemoMultidatasourceService demoMultidatasourceService;

    @ApiOperation(value = "创建数据(seata dubbo)")
    @PostMapping("/createAndGet")
    public Map<String, Object> createAndGet() {

        Map<String, Object> data = new HashMap<>();
        data.put("status", "done");
        try {
            data = this.demoMultidatasourceService.createAndGetData();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return data;
    }

    @ApiOperation(value = "获取数据")
    @PostMapping("/getData")
    public Map<String, Object> getData() {

        return this.demoMultidatasourceService.getData();
    }
}