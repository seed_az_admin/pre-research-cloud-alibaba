package gang.seed.az.demo.seata.multidatasource.mapper;


import gang.seed.az.demo.seata.multidatasource.config.datasource.CustomDataSource;
import gang.seed.az.demo.seata.multidatasource.domain.DemoSecondaryDo;
import gang.seed.az.demo.seata.multidatasource.mybatis.IBaseMapper;

/**
 * @Title: DemoSecondaryMapper
 * @Description: TODO
 * @author: duhang
 * @date: 2020-08-18
 */
@CustomDataSource("secondary")
public interface DemoSecondaryMapper extends IBaseMapper<DemoSecondaryDo> {

}