package gang.seed.az.demo.seata.multidatasource.mapper;


import gang.seed.az.demo.seata.multidatasource.domain.DemoPrimaryDo;
import gang.seed.az.demo.seata.multidatasource.mybatis.IBaseMapper;

/**
 * @Title: DemoPrimaryMapper
 * @Description: TODO
 * @author: duhang
 * @date: 2020-08-18
 */
public interface DemoPrimaryMapper extends IBaseMapper<DemoPrimaryDo> {

}