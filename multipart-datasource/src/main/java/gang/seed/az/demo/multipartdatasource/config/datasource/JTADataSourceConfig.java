package gang.seed.az.demo.multipartdatasource.config.datasource;

import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.env.Environment;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * Created by duhang on 2019/11/26 11:13
 */
@Configuration
@EnableTransactionManagement(proxyTargetClass = true)
public class JTADataSourceConfig extends AbsDataSourceConfig {

    /**
     * 主数据源
     * @return
     */
    @Primary
    @Bean(name = "dataSourcePrimary")
    public DataSource primary(Environment env) throws SQLException {
        String prefix = "spring.datasource.primary.";
        return getDataSource(env, prefix, "primary");
    }

    /**
     * 次数据源
     * @return
     */
    @Bean(name = "dataSourceSecondary")
    public DataSource secondary(Environment env) throws SQLException {
        String prefix = "spring.datasource.secondary.";
        return getDataSource(env, prefix, "secondary");
    }

    @Bean("dynamicDataSource")
    public DynamicDataSource dynamicDataSource(@Qualifier("dataSourcePrimary") DataSource dataSourcePrimary,
                                               @Qualifier("dataSourceSecondary") DataSource dataSourceSecondary) {
        Map<Object, Object> targetDataSources = new HashMap<>();
        targetDataSources.put("primary", dataSourcePrimary);
        targetDataSources.put("secondary", dataSourceSecondary);

        DynamicDataSource dataSource = new DynamicDataSource();
        dataSource.setTargetDataSources(targetDataSources);
        dataSource.setDefaultTargetDataSource(dataSourcePrimary);
        return dataSource;
    }

    @Bean(name = "sqlSessionFactoryPrimary")
    public SqlSessionFactory sqlSessionFactoryPrimary(@Qualifier("dataSourcePrimary") DataSource dataSource)
            throws Exception {
        return createSqlSessionFactory(dataSource);
    }

    @Bean(name = "sqlSessionFactorySecondary")
    public SqlSessionFactory sqlSessionFactorySecondary(@Qualifier("dataSourceSecondary") DataSource dataSource)
            throws Exception {
        return createSqlSessionFactory(dataSource);
    }

    @Bean(name = "sqlSessionTemplate")
    public CustomSqlSessionTemplate sqlSessionTemplate(@Qualifier("sqlSessionFactoryPrimary") SqlSessionFactory factoryPrimary,
                                                       @Qualifier("sqlSessionFactorySecondary") SqlSessionFactory factorySecondary) {
        Map<Object,SqlSessionFactory> sqlSessionFactoryMap = new HashMap<>();
        sqlSessionFactoryMap.put("primary", factoryPrimary);
        sqlSessionFactoryMap.put("secondary", factorySecondary);

        CustomSqlSessionTemplate customSqlSessionTemplate = new CustomSqlSessionTemplate(factoryPrimary);
        customSqlSessionTemplate.setDefaultTargetSqlSessionFactory(factoryPrimary);
        customSqlSessionTemplate.setTargetSqlSessionFactorys(sqlSessionFactoryMap);
        return customSqlSessionTemplate;
    }

    @Bean(name = "mapperScannerConfigurer")
    public MyMapperScannerConfigurer mapperScannerConfigurer() {
        MyMapperScannerConfigurer mapperScannerConfigurer = new MyMapperScannerConfigurer();
        mapperScannerConfigurer.setSqlSessionTemplateBeanName("sqlSessionTemplate");
//        mapperScannerConfigurer.setSqlSessionFactoryBeanName("sqlSessionFactory");
        mapperScannerConfigurer.setBasePackage("gang.seed.az.demo.**.mapper");
        // 配置通用Mapper，详情请查阅官方文档
        Properties properties = new Properties();
        properties.setProperty("mappers", "gang.seed.az.demo.orm.mybatis.IBaseMapper");
        properties.setProperty("type-aliases-package", "gang.seed.az.demo.**.domain");
        properties.setProperty("notEmpty", "true");//insert、update是否判断字符串类型!='' 即 test="str != null"表达式内是否追加 and str != ''
        properties.setProperty("IDENTITY", "MYSQL");
        mapperScannerConfigurer.setProperties(properties);
        return mapperScannerConfigurer;
    }

    private SqlSessionFactory createSqlSessionFactory(DataSource dataSource) throws Exception {
        SqlSessionFactoryBean bean = new SqlSessionFactoryBean();
        bean.setDataSource(dataSource);
        ResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
        bean.setConfigLocation(resolver.getResource("mybatis.xml"));
        bean.setMapperLocations(resolver.getResources("classpath:gang/seed/az/demo/**/**Mapper.xml"));
        return bean.getObject();
    }
}
