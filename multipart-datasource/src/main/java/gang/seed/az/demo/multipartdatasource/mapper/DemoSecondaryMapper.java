package gang.seed.az.demo.multipartdatasource.mapper;

import gang.seed.az.demo.multipartdatasource.config.datasource.CustomDataSource;
import gang.seed.az.demo.multipartdatasource.domain.DemoSecondaryDo;
import gang.seed.az.demo.orm.mybatis.IBaseMapper;

/**
 * @Title: DemoSecondaryMapper
 * @Description: TODO
 * @author: duhang
 * @date: 2020-08-18
 */
@CustomDataSource("secondary")
public interface DemoSecondaryMapper extends IBaseMapper<DemoSecondaryDo> {

}