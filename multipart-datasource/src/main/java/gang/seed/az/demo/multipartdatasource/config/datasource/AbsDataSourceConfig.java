package gang.seed.az.demo.multipartdatasource.config.datasource;


import com.mysql.jdbc.jdbc2.optional.MysqlXADataSource;
import org.springframework.boot.jta.atomikos.AtomikosDataSourceBean;
import org.springframework.core.env.Environment;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.Properties;

/**
 * 数据源配置抽象类
 *
 * Created by duhang on 2019/11/26 10:38
 */
public abstract class AbsDataSourceConfig {


    protected DataSource getDataSource(Environment env, String prefix, String dataSourceName) throws SQLException {

        Properties prop = build(env, prefix);

        MysqlXADataSource mysqlXaDataSource = new MysqlXADataSource();
        mysqlXaDataSource.setUrl(env.getProperty(prefix + "url"));
        mysqlXaDataSource.setPassword(env.getProperty(prefix + "password"));
        mysqlXaDataSource.setUser(env.getProperty(prefix + "username"));
        mysqlXaDataSource.setPinGlobalTxToPhysicalConnection(true);

        AtomikosDataSourceBean atomikosDataSourceBean = new AtomikosDataSourceBean();
        atomikosDataSourceBean.setXaDataSource(mysqlXaDataSource);
        atomikosDataSourceBean.setUniqueResourceName(dataSourceName);
        atomikosDataSourceBean.setXaProperties(prop);
        atomikosDataSourceBean.setMinPoolSize(0);
        atomikosDataSourceBean.setMaxPoolSize(30);
        atomikosDataSourceBean.setMaxLifetime(20000);
        atomikosDataSourceBean.setBorrowConnectionTimeout(30);
        atomikosDataSourceBean.setLoginTimeout(30);
        atomikosDataSourceBean.setMaintenanceInterval(60);
        atomikosDataSourceBean.setMaxIdleTime(60);
        atomikosDataSourceBean.setTestQuery("SELECT 1 FROM DUAL");

        return atomikosDataSourceBean;
    }

    protected Properties build(Environment env, String prefix) {
        Properties prop = new Properties();
        prop.put("url", env.getProperty(prefix + "url"));
        prop.put("username", env.getProperty(prefix + "username"));
        prop.put("password", env.getProperty(prefix + "password"));
//        prop.put("driverClassName", env.getProperty(prefix + "driverClassName", "com.mysql.jdbc.Driver"));
        prop.put("driverClassName", "com.mysql.jdbc.Driver");
        return prop;
    }
}
