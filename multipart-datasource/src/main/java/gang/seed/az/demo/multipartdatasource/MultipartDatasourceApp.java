package gang.seed.az.demo.multipartdatasource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MultipartDatasourceApp {

    private static final Logger LOGGER = LoggerFactory.getLogger(MultipartDatasourceApp.class);

    public static void main(String[] args) {

        SpringApplication.run(MultipartDatasourceApp.class);

        LOGGER.info(">>>>>>>>>>>>>>>>>>>>>>>>多数据源配置示例服务启动完成");
    }
}