package gang.seed.az.demo.multipartdatasource.config.datasource;

import java.lang.annotation.*;

/**
 * 数据源注解
 *         - 类及方法上
 * Created by duhang on 2019-09-10 14:44
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE, ElementType.METHOD})
public @interface CustomDataSource {
    String value() default "primary";
}
