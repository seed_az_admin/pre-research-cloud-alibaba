package gang.seed.az.demo.multipartdatasource.controller;

import gang.seed.az.demo.multipartdatasource.service.DemoMultipartDatasourceService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@Api(tags = {"多数据源示例api"})
@RestController
public class DemoMultipartDatasourceController {

    @Autowired
    private DemoMultipartDatasourceService demoMultipartDatasourceService;

    @ApiOperation(value = "获取量数据源全部")
    @GetMapping("/getAll")
    public Map<String, Object> getAll() {

        return this.demoMultipartDatasourceService.getAll();
    }

    @ApiOperation(value = "创建数据")
    @PostMapping("/createData")
    public Map<String, Object> createData() {

        return this.demoMultipartDatasourceService.createData();
    }
}