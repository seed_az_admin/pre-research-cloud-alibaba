package gang.seed.az.demo.multipartdatasource.config.datasource;

import org.apache.commons.lang3.reflect.FieldUtils;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.aop.framework.AopProxy;
import org.springframework.aop.framework.ProxyFactory;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.stereotype.Component;

import java.lang.reflect.Field;
import java.util.Objects;

/**
 * Created by duhang on 2019/11/26 10:44
 */
@Aspect
@Component
public class DataSourceAspect {

    protected static final ThreadLocal<String> S_HOLDER = new ThreadLocal<>();

    @Pointcut("execution( * gang.seed.az.demo..mapper..*.*(..))")
    protected void datasourceAspect() {

    }

    /**
     * 根据@CustomDataSource的value值设置不同的dataSourceKey,以供DynamicDataSource
     */
    @Before("datasourceAspect()")
    public void changeDataSourceBeforeMethodExecution(JoinPoint jp) throws IllegalAccessException {

        CustomDataSource targetDatasource = AnnotationUtils.findAnnotation(((MethodSignature) jp.getSignature()).getMethod(), CustomDataSource.class);
        if (Objects.isNull(targetDatasource)) {
            targetDatasource = AnnotationUtils.findAnnotation(jp.getClass(), CustomDataSource.class);
            if (Objects.isNull(targetDatasource)) {
                Object proxy = jp.getThis();
                Field h = FieldUtils.getDeclaredField(proxy.getClass().getSuperclass(), "h", true);
                AopProxy aopProxy = (AopProxy) h.get(proxy);
                ProxyFactory advised = (ProxyFactory) FieldUtils.readDeclaredField(aopProxy, "advised", true);
                Class<?> targetClass = advised.getProxiedInterfaces()[0];
                targetDatasource = AnnotationUtils.findAnnotation(targetClass, CustomDataSource.class);
            }
        }
        String key = null == targetDatasource ? "primary" : targetDatasource.value();
        S_HOLDER.set(DataSourceContextHolder.getDatasourceType());
        DataSourceContextHolder.setDatasourceType(key);

    }

    @After("datasourceAspect()")
    public void restoreDataSourceAfterMethodExecution() {
        DataSourceContextHolder.setDatasourceType(S_HOLDER.get());
        S_HOLDER.remove();
    }

}
