package gang.seed.az.demo.multipartdatasource.mapper;

import gang.seed.az.demo.multipartdatasource.domain.DemoPrimaryDo;
import gang.seed.az.demo.orm.mybatis.IBaseMapper;

/**
 * @Title: DemoPrimaryMapper
 * @Description: TODO
 * @author: duhang
 * @date: 2020-08-18
 */
public interface DemoPrimaryMapper extends IBaseMapper<DemoPrimaryDo> {

}