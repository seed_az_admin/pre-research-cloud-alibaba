package gang.seed.az.demo.multipartdatasource.service;

import gang.seed.az.demo.multipartdatasource.domain.DemoPrimaryDo;
import gang.seed.az.demo.multipartdatasource.mapper.DemoPrimaryMapper;
import gang.seed.az.demo.orm.mybatis.AbstractService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Title: DemoPrimaryService
 * @Description: TODO
 * @author: duhang
 * @date: 2020-08-18
 */
@Service
public class DemoPrimaryService extends AbstractService<DemoPrimaryDo> {

    @Autowired
    private DemoPrimaryMapper demoPrimaryMapper;
}