package gang.seed.az.demo.oauthinterface.vo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @Description TODO
 * @Author duhang
 * @Date 2020-08-17
 */
@Data
@Accessors(chain = true)
public class CustomerVo implements Serializable {

    private static final long serialVersionUID = -654093291117674571L;

    private String cusId;

    private String cusName;

    private String cusPassword;
}
