package gang.seed.az.demo.oauthinterface.service;

import gang.seed.az.demo.oauthinterface.vo.CustomerVo;

/**
 * @Description TODO
 * @Author duhang
 * @Date 2020-08-17
 */
public interface OauthDubboService {

    /**
     * 注册
     * @param name
     * @param password
     * @return
     */
    CustomerVo registered(String name, String password);

    /**
     * 登录
     * @param name
     * @param password
     * @return
     */
    String login(String name, String password);

    /**
     * 校验token
     * @param token
     * @return
     */
    CustomerVo checkToken(String token);
}
