package gang.seed.az.demo.buyer;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BuyerApp {

    private static final Logger LOGGER = LoggerFactory.getLogger(BuyerApp.class);

    public static void main(String[] args) {

        SpringApplication.run(BuyerApp.class);

        LOGGER.info(">>>>>>>>>>>>>>>>>>>>>>>>买家服务启动完成");
    }
}