package gang.seed.az.demo.buyer.controller;

import gang.seed.az.demo.buyer.annotation.AuthIgnore;
import gang.seed.az.demo.buyer.util.ThreadLocalUtil;
import gang.seed.az.demo.buyer.vo.GoodsDetailVo;
import gang.seed.az.demo.mallinterface.service.GoodsDubboService;
import gang.seed.az.demo.mallinterface.service.ShopDubboService;
import gang.seed.az.demo.mallinterface.vo.GoodsVo;
import gang.seed.az.demo.mallinterface.vo.SpecificationVo;
import gang.seed.az.demo.oauthinterface.service.OauthDubboService;
import gang.seed.az.demo.oauthinterface.vo.CustomerVo;
import gang.seed.az.demo.payinterface.service.OrderDubboService;
import gang.seed.az.demo.payinterface.vo.OrderDetailVo;
import gang.seed.az.demo.payinterface.vo.OrderVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.beans.BeanUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@Api(tags = {"买家api"})
@RestController
public class BuyerController {

    @Reference(version = "1.0.0", protocol = "dubbo", mock = "return null")
    private OauthDubboService oauthDubboService;
    @Reference(version = "1.0.0", protocol = "dubbo", mock = "return null")
    private OrderDubboService orderDubboService;
    @Reference(version = "1.0.0", protocol = "dubbo", mock = "return null")
    private GoodsDubboService goodsDubboService;
    @Reference(version = "1.0.0", protocol = "dubbo", mock = "return null")
    private ShopDubboService shopDubboService;

    @ApiOperation(value = "买家注册")
    @ApiImplicitParams(
            value = {
                    @ApiImplicitParam(
                            paramType = "form",
                            name = "name",
                            value = "买家用户名",
                            dataType = "String",
                            required = true
                    ),
                    @ApiImplicitParam(
                            paramType = "form",
                            name = "password",
                            value = "买家密码",
                            dataType = "String",
                            required = true
                    )
            }
    )
    @AuthIgnore
    @PostMapping("/registered")
    public String registered(String name, String password) {

        if (StringUtils.isNoneBlank(name, password)) {

            CustomerVo vo = this.oauthDubboService.registered(name, password);

            if (null != vo) {

                String token = this.oauthDubboService.login(name, password);

                return token;
            }
        }
        return null;
    }

    @ApiOperation(value = "买家登录")
    @ApiImplicitParams(
            value = {
                    @ApiImplicitParam(
                            paramType = "form",
                            name = "name",
                            value = "买家用户名",
                            dataType = "String",
                            required = true
                    ),
                    @ApiImplicitParam(
                            paramType = "form",
                            name = "password",
                            value = "买家密码",
                            dataType = "String",
                            required = true
                    )
            }
    )
    @AuthIgnore
    @PostMapping("/login")
    public String login(String name, String password) {

        if (StringUtils.isNoneBlank(name, password)) {

            String token = this.oauthDubboService.login(name, password);

            return token;
        }
        return null;
    }

    @ApiOperation(value = "买买买")
    @PostMapping("/buy")
    public OrderVo buy(@RequestBody List<GoodsDetailVo> detailVoList) {

        if (CollectionUtils.isNotEmpty(detailVoList)) {

            String buyerId = ThreadLocalUtil.getUserId();

            List<OrderDetailVo> goodsDetailList = new ArrayList<>(detailVoList.size());

            detailVoList.forEach(detail -> {

                OrderDetailVo orderDetailVo = new OrderDetailVo();
                BeanUtils.copyProperties(detail, orderDetailVo);

                String specId = orderDetailVo.getSpecId();
                SpecificationVo specificationVo = this.goodsDubboService.getSpecById(specId);

                String goodsId = specificationVo.getGoodsId();
                GoodsVo goodsVo = this.goodsDubboService.getGoodsById(goodsId);

                orderDetailVo.setGoodsId(goodsId).setShopId(goodsVo.getShopId());

                goodsDetailList.add(orderDetailVo);
            });
            OrderVo orderVo = this.orderDubboService.pay(goodsDetailList, buyerId);

            return orderVo;
        }
        return null;
    }

    @ApiOperation(value = "买家获取我的订单")
    @GetMapping("/getOrderList")
    public List<OrderVo> getOrderList() {

        String buyerId = ThreadLocalUtil.getUserId();

        List<OrderVo> orderVoList = this.orderDubboService.getOrderList(buyerId);

        return orderVoList;
    }
}