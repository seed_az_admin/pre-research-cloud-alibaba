package gang.seed.az.demo.buyer.interceptor;

import gang.seed.az.demo.buyer.annotation.AuthIgnore;
import gang.seed.az.demo.buyer.util.ThreadLocalUtil;
import gang.seed.az.demo.oauthinterface.service.OauthDubboService;
import gang.seed.az.demo.oauthinterface.vo.CustomerVo;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @Title AbstractAuthInterceptor
 * @Description: 通用抽象权限拦截器
 * @author: duhang
 * @date: 2019/8/16 8:59
 */
@Component
public class AuthInterceptor implements HandlerInterceptor {

    @Autowired
    private OauthDubboService oauthDubboService;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        if (request.getMethod().equalsIgnoreCase("options")) {
            return Boolean.TRUE;
        }
        // 获取request头信息中的访问标识符以及token信息
        String token = request.getHeader(HttpHeaders.AUTHORIZATION);
        token = StringUtils.isNotBlank(token) ? token : request.getParameter(HttpHeaders.AUTHORIZATION);

        // 有AuthIgnore注解的不验证
        if (handler instanceof HandlerMethod) {

            AuthIgnore authIgnore = ((HandlerMethod) handler).getMethodAnnotation(AuthIgnore.class);

            if (null != authIgnore) {

                return Boolean.TRUE;
            }
        }

        CustomerVo vo = this.oauthDubboService.checkToken(token);

        boolean returnFlag = null != vo;

        if (returnFlag) {

            ThreadLocalUtil.set(vo);
        }
        return returnFlag;
    }


    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        HandlerInterceptor.super.postHandle(request, response, handler, modelAndView);
        MDC.clear();
    }
}
