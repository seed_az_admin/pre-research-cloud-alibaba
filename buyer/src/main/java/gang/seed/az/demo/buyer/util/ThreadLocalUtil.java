package gang.seed.az.demo.buyer.util;

import gang.seed.az.demo.oauthinterface.vo.CustomerVo;

/**
 * @Description TODO
 * @Author duhang
 * @Date 2020-08-17
 */
public class ThreadLocalUtil {

    private static ThreadLocal<CustomerVo> S_LOGIN = new ThreadLocal<>();

    public static void set(CustomerVo vo) {

        if (null != vo) {

            S_LOGIN.set(vo);
        }
    }

    public static CustomerVo get() {

        return S_LOGIN.get();
    }

    public static String getUserId() {

        CustomerVo vo = get();

        if (null != vo) {

            return vo.getCusId();
        }
        return null;
    }
}
