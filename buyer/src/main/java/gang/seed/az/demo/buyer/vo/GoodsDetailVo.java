package gang.seed.az.demo.buyer.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Data
@Accessors(chain = true)
@ApiModel(value = "订单内容vo")
public class GoodsDetailVo implements Serializable {

    private static final long serialVersionUID = 727771224676946448L;

    @ApiModelProperty(required = true, value = "规格id")
    private String specId;

    @ApiModelProperty(required = true, value = "数量")
    private Integer quantity;
}
