package gang.seed.az.demo.seata.singledatasource;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SeataSingleDatasourceApp {

    private static final Logger LOGGER = LoggerFactory.getLogger(SeataSingleDatasourceApp.class);

    public static void main(String[] args) {

        SpringApplication.run(SeataSingleDatasourceApp.class);

        LOGGER.info(">>>>>>>>>>>>>>>>>>>>>>>>seata单数据源启动完成");
    }
}