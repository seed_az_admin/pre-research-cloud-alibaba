package gang.seed.az.demo.seata.singledatasource.service;

import gang.seed.az.demo.seata.singledatasource.domain.DemoPrimaryDo;
import gang.seed.az.demo.seata.singledatasource.mapper.DemoPrimaryMapper;
import gang.seed.az.demo.seata.singledatasource.mybatis.AbstractService;
import io.seata.spring.annotation.GlobalTransactional;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Title: DemoPrimaryService
 * @Description: TODO
 * @author: duhang
 * @date: 2020-08-18
 */
@Service
public class DemoPrimaryService extends AbstractService<DemoPrimaryDo> {

    @Autowired
    private DemoPrimaryMapper demoPrimaryMapper;
    @Reference
    private DemoSecondarySingleDubboService demoSecondarySingleDubboService;

    @GlobalTransactional(rollbackFor = Exception.class)
    public void createData() {

        this.insert(new DemoPrimaryDo().setPrimaryContent("primary start"));

        this.demoSecondarySingleDubboService.createData("seata dubbo");

        this.insert(new DemoPrimaryDo().setPrimaryContent("primary end"));

//        throw new RuntimeException("测试seata事务");
    }

    @GlobalTransactional(rollbackFor = Exception.class)
    public void createDataPrimary() {

        this.insert(new DemoPrimaryDo().setPrimaryContent("primary start"));
        this.insert(new DemoPrimaryDo().setPrimaryContent("primary self"));
        this.insert(new DemoPrimaryDo().setPrimaryContent("primary end"));
    }

}