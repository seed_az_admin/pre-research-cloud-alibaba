package gang.seed.az.demo.seata.singledatasource.mapper;


import gang.seed.az.demo.seata.singledatasource.domain.DemoPrimaryDo;
import gang.seed.az.demo.seata.singledatasource.mybatis.IBaseMapper;

/**
 * @Title: DemoPrimaryMapper
 * @Description: TODO
 * @author: duhang
 * @date: 2020-08-18
 */
public interface DemoPrimaryMapper extends IBaseMapper<DemoPrimaryDo> {

}