package gang.seed.az.demo.seata.singledatasource.service;

import gang.seed.az.demo.seata.singledatasource.domain.DemoPrimaryDo;
import gang.seed.az.demo.seata.singledatasource.vo.DemoPrimarySingleVo;
import io.seata.spring.annotation.GlobalTransactional;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @Title: DemoPrimarySingleDubboServiceImpl
 * @Description: TODO
 * @author: duhang
 * @date: 2020-08-20
 */
@Service(timeout = 60 * 1000)
public class DemoPrimarySingleDubboServiceImpl implements DemoPrimarySingleDubboService {

    @Autowired
    private DemoPrimaryService demoPrimaryService;

    @GlobalTransactional(rollbackFor = Exception.class)
    @Override
    public DemoPrimarySingleVo createData(String content) {

        DemoPrimaryDo demoPrimaryDo = new DemoPrimaryDo().setPrimaryContent(content);

        DemoPrimarySingleVo vo = null;

        int count = this.demoPrimaryService.insert(demoPrimaryDo);

        if (count > 0) {

            vo = new DemoPrimarySingleVo();
            BeanUtils.copyProperties(demoPrimaryDo, vo);
        }
        return vo;
    }
}