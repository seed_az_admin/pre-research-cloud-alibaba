package gang.seed.az.demo.seata.singledatasource.controller;

import gang.seed.az.demo.seata.singledatasource.service.DemoPrimaryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = {"seata单数据源示例api"})
@RestController
public class DemoPrimaryController {

    @Autowired
    private DemoPrimaryService demoPrimaryService;

    @ApiOperation(value = "创建数据(seata dubbo)")
    @PostMapping("/createDataSeataDubbo")
    public String createDataSeataDubbo() {

        this.demoPrimaryService.createData();

        return "done";
    }

    @ApiOperation(value = "创建数据(自己)")
    @PostMapping("/createDataSelf")
    public String createDataSelf() {

        this.demoPrimaryService.createDataPrimary();

        return "done";
    }
}