package gang.seed.az.demo.seata.singledatasource.vo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @Title: DemoSecondarySingleVo
 * @Description: TODO
 * @author: duhang
 * @date: 2020-08-20
 */
@Data
@Accessors(chain = true)
public class DemoSecondarySingleVo implements Serializable {

    private static final long serialVersionUID = -1773895038158418415L;

    private Integer secondaryId;

    private String secondaryContent;
}