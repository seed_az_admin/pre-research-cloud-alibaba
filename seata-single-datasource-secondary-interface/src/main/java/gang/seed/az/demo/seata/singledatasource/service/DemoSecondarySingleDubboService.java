package gang.seed.az.demo.seata.singledatasource.service;

import gang.seed.az.demo.seata.singledatasource.vo.DemoSecondarySingleVo;

/**
 * @Title: DemoSecondarySingleDubboService
 * @Description: TODO
 * @author: duhang
 * @date: 2020-08-20
 */
public interface DemoSecondarySingleDubboService {

    DemoSecondarySingleVo createData(String content);
}