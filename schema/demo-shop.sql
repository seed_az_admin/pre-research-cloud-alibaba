/*
Navicat MySQL Data Transfer

Source Server         : 192.168.106.50_3308
Source Server Version : 50726
Source Host           : 192.168.106.50:3308
Source Database       : demo-shop

Target Server Type    : MYSQL
Target Server Version : 50726
File Encoding         : 65001

Date: 2020-08-23 22:30:45
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for demo_goods
-- ----------------------------
DROP TABLE IF EXISTS `demo_goods`;
CREATE TABLE `demo_goods` (
  `goods_id` char(24) NOT NULL,
  `goods_name` varchar(255) NOT NULL,
  `shop_id` char(24) NOT NULL,
  PRIMARY KEY (`goods_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for demo_shop
-- ----------------------------
DROP TABLE IF EXISTS `demo_shop`;
CREATE TABLE `demo_shop` (
  `shop_id` char(24) NOT NULL,
  `shop_name` varchar(255) NOT NULL,
  `selled_id` char(24) NOT NULL,
  PRIMARY KEY (`shop_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for demo_specification
-- ----------------------------
DROP TABLE IF EXISTS `demo_specification`;
CREATE TABLE `demo_specification` (
  `spec_id` char(24) NOT NULL,
  `spec_name` varchar(255) NOT NULL,
  `unit_price` decimal(10,3) NOT NULL,
  `goods_id` char(24) NOT NULL,
  PRIMARY KEY (`spec_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
