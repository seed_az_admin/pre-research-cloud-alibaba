/*
Navicat MySQL Data Transfer

Source Server         : 192.168.106.50_3308
Source Server Version : 50726
Source Host           : 192.168.106.50:3308
Source Database       : demo-datasource-secondary

Target Server Type    : MYSQL
Target Server Version : 50726
File Encoding         : 65001

Date: 2020-08-23 22:30:30
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for demo_primary
-- ----------------------------
DROP TABLE IF EXISTS `demo_primary`;
CREATE TABLE `demo_primary` (
  `primary_id` int(11) NOT NULL AUTO_INCREMENT,
  `primary_content` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`primary_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for demo_secondary
-- ----------------------------
DROP TABLE IF EXISTS `demo_secondary`;
CREATE TABLE `demo_secondary` (
  `secondary_id` int(11) NOT NULL AUTO_INCREMENT,
  `secondary_content` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`secondary_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for undo_log
-- ----------------------------
DROP TABLE IF EXISTS `undo_log`;
CREATE TABLE `undo_log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `branch_id` bigint(20) NOT NULL,
  `xid` varchar(100) NOT NULL,
  `context` varchar(128) NOT NULL,
  `rollback_info` longblob NOT NULL,
  `log_status` int(11) NOT NULL,
  `log_created` datetime NOT NULL,
  `log_modified` datetime NOT NULL,
  `ext` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_undo_log` (`xid`,`branch_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
