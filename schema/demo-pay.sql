/*
Navicat MySQL Data Transfer

Source Server         : 192.168.106.50_3308
Source Server Version : 50726
Source Host           : 192.168.106.50:3308
Source Database       : demo-pay

Target Server Type    : MYSQL
Target Server Version : 50726
File Encoding         : 65001

Date: 2020-08-23 22:30:37
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for demo_order
-- ----------------------------
DROP TABLE IF EXISTS `demo_order`;
CREATE TABLE `demo_order` (
  `order_id` char(24) NOT NULL,
  `buyer_id` char(24) NOT NULL,
  `total_price` decimal(10,3) NOT NULL,
  PRIMARY KEY (`order_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for demo_order_detail
-- ----------------------------
DROP TABLE IF EXISTS `demo_order_detail`;
CREATE TABLE `demo_order_detail` (
  `detail_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` char(24) NOT NULL,
  `goods_id` char(24) NOT NULL,
  `spec_id` char(24) NOT NULL,
  `quantity` int(11) NOT NULL,
  `shop_id` char(24) NOT NULL,
  PRIMARY KEY (`detail_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
