/*
Navicat MySQL Data Transfer

Source Server         : 192.168.106.50_3308
Source Server Version : 50726
Source Host           : 192.168.106.50:3308
Source Database       : demo-customer

Target Server Type    : MYSQL
Target Server Version : 50726
File Encoding         : 65001

Date: 2020-08-23 22:30:15
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for demo_customer
-- ----------------------------
DROP TABLE IF EXISTS `demo_customer`;
CREATE TABLE `demo_customer` (
  `cus_id` char(24) NOT NULL,
  `cus_name` varchar(255) NOT NULL,
  `cus_password` varchar(255) NOT NULL,
  PRIMARY KEY (`cus_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
